import "ag-grid-enterprise";
import React, { useState, useRef, useEffect, useCallback } from "react";
import { AgGridReact } from "ag-grid-react/lib/agGridReact";
import "ag-grid-community/styles/ag-grid.css"; // Core grid CSS, always needed
import "ag-grid-community/styles/ag-theme-alpine.css"; // Optional theme CSS
import { columnTypes } from "./utils/ColumnTypes";
import { Pagination, Select } from "antd";
import { NetWorkService, validResponse } from "util/API";
import { CODE_SUCCESS } from "util/Constant";

const AgTable = (props: any) => {
  const { url, columnDefs, cellClicked, animateRows, defaultSort } = props;

  const { Get } = NetWorkService;

  // aggrid
  const gridRef: any = useRef(); // Optional - for accessing Grid's API
  const [gridApi, setGridApi] = useState<any>(null);
  // current page
  const [currentPage, setCurrentPage] = useState<number>(1);
  const onChangeCurrentPage = (e: any) => {
    setCurrentPage(e);
    if (gridApi) gridApi.paginationGoToPage(e - 1);
  };

  // page size
  const [pageSize, setPageSize] = useState(20);
  const onChangePageSize = (e: any) => {
    setPageSize(e);
    gridApi.paginationSetPageSize(e);
  };

  // total row
  const [totalRow, setTotalRow] = useState(0);
  //--------------------//
  const setDefaultSort = (gridColumnApi: any) => {
    if (defaultSort && gridColumnApi) {
      gridColumnApi.applyColumnState({
        state: [defaultSort],
        defaultState: { sort: null },
      });
    }
  };

  const ServerSideDatasource = (url: string, typeQuery?: string) => {
    return {
      async getRows(params: any) {
        try {
          const { sortModel, filterModel, endRow } = params.request;
          let query = url;
          // pagination
          query += `?limit=${pageSize}&offset=${endRow - pageSize}`;
          // sort

          let res: any;
          res = await Get({ url: url });

          if (res && validResponse(res) && res.code === CODE_SUCCESS) {
            const respons = res.data;
            setTotalRow(
              respons?.data.pagination?.totalRows || respons.data.length || 0
            );
            if (respons.data.records.length !== 0) {
              params.success({
                rowData: respons.data.records || [],
                rowCount:
                  respons.data.pagination?.totalRows ||
                  respons.data.records.length ||
                  0,
              });
            } else {
              params.success({
                rowData: [],
                rowCount: 0,
              });
            }
          }
        } catch (error) {}
      },
    };
  };
  const onGridReady = useCallback(
    (params: any) => {
      const { api, columnApi } = params;
      setGridApi(api);
      setDefaultSort(columnApi);
      // api.setServerSideDatasource(ServerSideDatasource(url));
    },
    // eslint-disable-next-line
    []
  );

  useEffect(() => {
    if (gridApi) {
      gridApi.setServerSideDatasource(ServerSideDatasource(url));
    }
    // eslint-disable-next-line
  }, [gridApi, pageSize, url]);

  return (
    <div className="ag-theme-alpine" style={{ width: "100%", height: 500 }}>
      <AgGridReact
        ref={gridRef}
        columnTypes={columnTypes()}
        defaultColDef={{
          flex: 1,
          // minWidth: 100,
          headerClass: "ag-table-header",
          cellClass: "ag-table-cell",
          menuTabs: ["filterMenuTab"],
          filterParams: {
            suppressAndOrCondition: true,
          },
        }}
        onGridReady={onGridReady}
        // server side
        rowModelType={"serverSide"}
        serverSideStoreType={"partial"}
        cacheBlockSize={pageSize}
        // animate
        animateRows={true}
        //-----style-----//
        headerHeight={40}
        rowHeight={37}
        rowClass="ag-table-row"
        //---------------//

        //-----pagination
        pagination={true}
        paginationPageSize={pageSize}
        suppressPaginationPanel={true}
        // custom component
        rowSelection={"single"}
        suppressContextMenu={true}
        columnDefs={columnDefs}
      ></AgGridReact>
      <div className="aggrid-footer">
        <div className="custom-pagination-container">
          <Pagination
            className="custom-pagination"
            current={currentPage}
            showSizeChanger={false}
            total={totalRow}
            pageSize={pageSize}
            onChange={onChangeCurrentPage}
          ></Pagination>
        </div>

        <div className="custom-pagination-changePageSize">
          <div className={"change-pagesize-title"}>Hiển thị</div>
          <Select value={pageSize} onChange={onChangePageSize}>
            <Select.Option value={1}>1</Select.Option>
            <Select.Option value={2}>2</Select.Option>
            <Select.Option value={3}>3</Select.Option>
          </Select>
        </div>
      </div>
    </div>
  );
};

export default AgTable;
