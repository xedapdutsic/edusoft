import moment from "moment";

export const columnTypes = () => ({
  textFilter: {
    filter: "agTextColumnFilter",
    filterParams: {
      suppressAndOrCondition: true,
      debounceMs: 500,
      filterOptions: [
        {
          displayKey: "contains",
          displayName: "Bao gồm",
        },
      ],
    },
  },

  numberFilter: {
    filter: "agNumberColumnFilter",
    filterParams: {
      suppressAndOrCondition: true,
      debounceMs: 500,
      filterOptions: [
        {
          displayKey: "equals",
          displayName: "Equals",
          test: function (filterValue: any, cellValue: any) {
            console.log(111);

            return cellValue === filterValue;
          },
        },
        {
          displayKey: "greaterThan",
          displayName: "Greater than",
          test: function (filterValue: any, cellValue: any) {
            return cellValue > filterValue;
          },
        },
        {
          displayKey: "greaterThanOrEqual",
          displayName: "Greater than or equal",
          test: function (filterValue: any, cellValue: any) {
            return cellValue >= filterValue;
          },
        },
        {
          displayKey: "lessThanOrEqual",
          displayName: "Less than or equal",
          test: function (filterValue: any, cellValue: any) {
            return cellValue <= filterValue;
          },
        },
        {
          displayKey: "lessThan",
          displayName: "Less than",
          test: function (filterValue: any, cellValue: any) {
            return cellValue < filterValue;
          },
        },
        {
          displayKey: "inRange",
          displayName: "In range",
          numberOfInputs: 2,
          test: function (filterValues: any, cellValue: any) {
            var minValue = filterValues[0];
            var maxValue = filterValues[1];
            return (
              cellValue == null ||
              (cellValue >= minValue && cellValue <= maxValue)
            );
          },
        },
      ],
    },
  },

  dateFilter: {
    filter: "agDateColumnFilter",
    filterParams: {
      // provide comparator function
      comparator: (filterLocalDateAtMidnight: any, cellValue: any) => {
        const dateAsString = cellValue;

        if (dateAsString == null) {
          return 0;
        }

        // In the example application, dates are stored as dd/mm/yyyy
        // We create a Date object for comparison against the filter date
        const dateParts = dateAsString.split("/");
        const year = Number(dateParts[2]);
        const month = Number(dateParts[1]) - 1;
        const day = Number(dateParts[0]);
        const cellDate = new Date(year, month, day);

        // Now that both parameters are Date objects, we can compare
        if (cellDate < filterLocalDateAtMidnight) {
          return -1;
        } else if (cellDate > filterLocalDateAtMidnight) {
          return 1;
        }
        return 0;
      },
    },
  },
});
