import { Box } from "@chakra-ui/react";
import { Modal } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import Cloes from "views/admin/OrganizationManagement/Icon/Cloes";
import FormAddUser from "./FormAddUser";

interface p {
  onClose: () => void;
  isOpen: boolean;
}

const ModalAddUser = ({ onClose, isOpen }: p) => {
  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  return (
    <Modal
      title="Thêm mới người dùng Edubills"
      open={isOpen}
      onCancel={onClose}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Box px="16px" py="16px">
        <FormAddUser onFinish={onFinish} onClose={onClose} />
      </Box>
    </Modal>
  );
};

export default ModalAddUser;
