import { Box, Flex } from "@chakra-ui/react";
import { Button, Form, Input, Select } from "antd";
import MainButton from "components/button";
import React, { FC } from "react";
const { Option } = Select;

interface IProps {
  onFinish: (values: any) => void;
  onClose: () => void;
}

const FormAddUser: FC<IProps> = ({ onFinish, onClose }: IProps) => {
  return (
    <Form
      name="basic"
      onFinish={onFinish}
      autoComplete="off"
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
    >
      <Form.Item
        labelAlign="left"
        label="Họ và tên"
        name="username"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Mã nhân sự"
        name="username2"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Số điện thoại"
        name="username3"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Email"
        name="username4"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>

      <Form.Item
        name="gender"
        labelAlign="left"
        label="Quyền"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Select placeholder="Chức năng" allowClear className="w-custom">
          <Option value="male">male</Option>
          <Option value="female">female</Option>
          <Option value="other">other</Option>
        </Select>
      </Form.Item>

      <Flex flexDirection={"row"} alignContent="center" justifyContent="center">
        <Button
          htmlType="submit"
          className="custom-button button-dash"
          onClick={onClose}
          style={{
            width: "95px",
          }}
        >
          Hủy
        </Button>
        <Box ml="2">
          <MainButton htmlType="submit" text="Xác nhận" />
        </Box>
      </Flex>
    </Form>
  );
};

export default FormAddUser;
