import { Box } from "@chakra-ui/react";
import { Button, Form, Input, Select } from "antd";
import MainButton from "components/button";
import React from "react";

const { Option } = Select;
const HeaderSearch = () => {
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };
  return (
    <Box width={"100%"}>
      <Form onFinish={onFinish} form={form} style={{ maxWidth: "100%" }}>
        <Box display="flex" flexWrap="wrap">
          <Box mr="2">
            <Form.Item name="username1" className="wrap-input">
              <Input placeholder="Mã nhân sự" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username">
              <Input placeholder="Họ và tên" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="gender">
              <Select placeholder="Chức năng" allowClear className="">
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>
          </Box>
          <Box mr={2}>
            <Form.Item>
              <MainButton htmlType="submit" text="Tìm kiếm" />
            </Form.Item>
          </Box>

          <Form.Item>
            <Button
              htmlType="submit"
              className="custom-button button-danger"
              danger
            >
              Bỏ lọc
            </Button>
          </Form.Item>
        </Box>
      </Form>
    </Box>
  );
};

export default HeaderSearch;
