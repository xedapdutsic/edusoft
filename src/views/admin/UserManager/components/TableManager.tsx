import { Box, Button, Flex, Text, useDisclosure } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ModalAddUser from "./ModalAddUser";
import ModalUserManagement from "./ModalUserManagement";

interface DataType {
  key: string;
  stt: string;
  name: string;
  organizationCode: string;
  phoneNumber: string;
  email: string;
  permission: string;
}

const current = 1;
const pageSize = 2;

const TableManager = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [visible, setVisible] = useState(false);
  const history = useHistory();

  const [dataSource, setDataSource] = useState<DataType[]>([]);
  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  const handleNavigate = (id: string) => {
    history.push(`/admin/user-detail/${id}`);
  };

  const onHandleClose = () => {
    setVisible(false);
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Họ và tên",
      dataIndex: "name",
      key: "organizationName",
      render: (text, value) => (
        <Text
          onClick={() => handleNavigate(value.organizationCode)}
          cursor="pointer"
          style={{
            color: "#045993",
          }}
        >
          {text}
        </Text>
      ),
    },
    {
      title: "Mã tổ chức",
      dataIndex: "organizationCode",
      key: "organizationCode",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Quyền",
      dataIndex: "permission",
      key: "permission",
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      email: "tuanhanh@gmail.com",
      organizationCode: "1234567890",
      name: "Pisa",
      phoneNumber: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "1",
    },
    {
      key: "2",
      email: "tuanhanh@gmail.com",
      organizationCode: "1234567890",
      name: "Pisa",
      phoneNumber: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "2",
    },
    {
      key: "3",
      email: "tuanhanh@gmail.com",
      organizationCode: "1234567890",
      name: "Pisa",
      phoneNumber: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "3",
    },
  ];

  useEffect(() => {
    setDataSource(data);
  }, []);

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: 50 người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
          onClick={onOpen}
        >
          Thêm mới
        </Button>
      </Flex>

      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />

      <ModalAddUser isOpen={isOpen} onClose={onClose} />
      <ModalUserManagement isOpen={visible} onClose={onHandleClose} />
    </Box>
  );
};

export default TableManager;
