import { Box } from "@chakra-ui/react";
import { HSeparator } from "components/separator/Separator";
import { globalStyles } from "theme/styles";
import HeaderSearch from "./components/HeaderSearch";
import TableManager from "./components/TableManager";

export default function UserManager() {
  // Chakra Color Mode
  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
    >
      <HeaderSearch />
      <HSeparator />
      <TableManager />
    </Box>
  );
}
