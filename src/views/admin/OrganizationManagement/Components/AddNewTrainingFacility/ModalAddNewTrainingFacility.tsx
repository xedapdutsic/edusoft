import { Box, Flex, Text } from "@chakra-ui/react";
import { Button, FormInstance, Modal } from "antd";
import MainButton from "components/button";
import { HSeparator } from "components/separator/Separator";
import React, { useState } from "react";
import { globalStyles } from "theme/styles";
import Cloes from "../../Icon/Cloes";
import Plus from "../../Icon/Plus";
import ModalAddNewTrainingFacility2 from "../AddNewTrainingFacility2/ModalAddNewTrainingFacility2";
import ModalSetPaymentPartner from "../SetPaymentPartner/ModalSetPaymentPartner";
import ItemNewTrainingFacility from "./ItemNewTrainingFacility";

interface p {
  onClose: () => void;
  isOpen: boolean;
  onReset: () => void;
}

const ModalAddNewTrainingFacility = ({ onClose, isOpen, onReset }: p) => {
  const formRef = React.useRef<FormInstance>(null);

  const [isVisibleTrainingFacility2, setIsVisibleTrainingFacility2] =
    useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const [trainingFacilitys, setTrainingFacilitys] = useState<
    { name: string; visible: boolean }[]
  >([]);

  const onFinish = (values: any) => {
    console.log("Success:", values);
    setTrainingFacilitys([
      ...trainingFacilitys,
      { name: `${values.organizationName_2}`, visible: false },
    ]);
    setIsVisible(true);
    setIsVisibleTrainingFacility2(false);
    handleResetField();
  };

  const handleResetField = () => {
    formRef?.current?.setFieldsValue({
      Fax_2: "",
      Email_2: "",
      address_2: "",
      phoneNumber_2: "",
      email_login_2: "",
      address_login_2: "",
      username_login_2: "",
      organizationCode_2: "",
      organizationName_2: "",
      phoneNumber_login_2: "",
    });
  };

  const handleOnClickMainButton = () => {
    onClose();
    onReset();
    setTrainingFacilitys([]);
  };

  const handleSetVisible = () => {
    setTrainingFacilitys([
      ...trainingFacilitys,
      {
        name: trainingFacilitys[trainingFacilitys.length - 1].name,
        visible: true,
      },
    ]);
  };

  return (
    <Modal
      title="Thêm mới cơ sở đào tạo"
      open={isOpen}
      onCancel={onClose}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Flex
        px="16px"
        py="16px"
        flexDirection={"column"}
        justifyContent="center"
        alignContent="center"
      >
        <Button
          type="dashed"
          className="dashed-button"
          onClick={() => setIsVisibleTrainingFacility2(true)}
        >
          <Flex>
            <Plus />
            <Text
              color={globalStyles.colors.text["00AEEF"]}
              fontSize="14px"
              fontWeight={400}
              pl="2"
            >
              Thêm cơ sở đào tạo
            </Text>
          </Flex>
        </Button>

        <Box>
          {trainingFacilitys.map((e, i) => (
            <ItemNewTrainingFacility
              name={e.name}
              key={i}
              visible={e.visible}
            />
          ))}
        </Box>
        <Flex
          mt={2}
          flexDirection={"row"}
          alignContent="center"
          justifyContent="center"
        >
          <Button
            className="custom-button button-dash"
            onClick={() => {
              onClose();
              setTrainingFacilitys([]);
            }}
            style={{
              width: "95px",
            }}
          >
            Hủy
          </Button>
          <Box ml="2">
            <MainButton
              htmlType="submit"
              text="Xác nhận"
              onClick={handleOnClickMainButton}
              disabled={trainingFacilitys.every((e) => e.visible)}
            />
          </Box>
        </Flex>
      </Flex>
      <ModalAddNewTrainingFacility2
        formRef={formRef}
        isOpen={isVisibleTrainingFacility2}
        onClose={() => {
          setIsVisibleTrainingFacility2(false);
        }}
        onFinish={onFinish}
        setTrainingFacilitys={() => setTrainingFacilitys([])}
      />
      <ModalSetPaymentPartner
        isOpen={isVisible}
        onClose={() => setIsVisible(false)}
        handleSetVisible={handleSetVisible}
      />
    </Modal>
  );
};

export default ModalAddNewTrainingFacility;
