import { Box, Text } from "@chakra-ui/react";
import React from "react";
import { globalStyles } from "theme/styles";
import Check from "../../Icon/Check";

interface P {
  name: string;
  key: string | number;
  visible: boolean;
}

const ItemNewTrainingFacility = ({ name, key, visible }: P) => {
  return (
    <Box
      key={key}
      border={"1px"}
      borderColor={globalStyles.colors.text[200]}
      backgroundColor="#B8EBFF"
      borderRadius="20px"
      display={visible ? "flex" : "none"}
      alignItems="center"
      pl="17px"
      w={"75%"}
      mb="15px"
      mx="auto"
      h="52px"
    >
      <Check />
      <Text color={globalStyles.colors.text[200]} pl="2">
        {name}
      </Text>
    </Box>
  );
};

export default ItemNewTrainingFacility;
