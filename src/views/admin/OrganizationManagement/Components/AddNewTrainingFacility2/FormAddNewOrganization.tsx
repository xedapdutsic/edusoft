import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";
import { Button, Form, FormInstance } from "antd";
import MainButton from "components/button";
import Organization from "./Organization";
import { globalStyles } from "theme/styles";
import LoginAccount from "./LoginAccount";

interface IProps {
  onFinish: (values: any) => void;
  onClose: () => void;
  formRef: React.MutableRefObject<FormInstance<any>>;
}

const FormAddNewOrganization = ({ onFinish, onClose, formRef }: IProps) => {
  const onReset = () => {
    formRef?.current?.setFieldsValue({
      Fax_2: "",
      Email_2: "",
      address_2: "",
      phoneNumber_2: "",
      email_login_2: "",
      address_login_2: "",
      username_login_2: "",
      organizationCode_2: "",
      organizationName_2: "",
      phoneNumber_login_2: "",
    });
  };
  return (
    <Form
      ref={formRef}
      name="FormAddNewOrganization2"
      onFinish={onFinish}
      autoComplete="off"
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
    >
      <Organization />
      <Text
        mt="3"
        mb="5"
        fontSize={"15px"}
        fontWeight="600"
        color={globalStyles.colors.text["4A4A4A"]}
      >
        Tài khoản đăng nhập
      </Text>
      <LoginAccount />
      <Flex flexDirection={"row"} alignContent="center" justifyContent="center">
        <Button
          className="custom-button button-dash"
          onClick={() => {
            onClose();
            onReset();
          }}
          style={{
            width: "95px",
          }}
        >
          Hủy
        </Button>
        <Box ml="2">
          <MainButton htmlType="submit" text="Tiếp tục" />
        </Box>
      </Flex>
    </Form>
  );
};

export default FormAddNewOrganization;
