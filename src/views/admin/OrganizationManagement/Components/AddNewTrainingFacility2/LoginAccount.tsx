import { Box } from "@chakra-ui/react";
import { Form, Input } from "antd";
import React from "react";

const LoginAccount = () => {
  return (
    <Box>
      <Form.Item
        labelAlign="left"
        label="Username"
        name="username_login_2"
        rules={[{ required: true, message: "Vui lòng nhập username!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Họ và tên"
        name="fullName_login_2"
        rules={[{ required: true, message: "Vui lòng nhập họ và tên!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Số điện thoại"
        name="phoneNumber_login_2"
        rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Email"
        name="email_login_2"
        rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Địa chỉ"
        name="address_login_2"
        rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
    </Box>
  );
};

export default LoginAccount;
