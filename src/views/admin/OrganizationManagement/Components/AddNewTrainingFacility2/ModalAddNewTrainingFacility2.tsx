import { Box } from "@chakra-ui/react";
import { FormInstance, Modal } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import Cloes from "../../Icon/Cloes";
import FormAddNewOrganization from "./FormAddNewOrganization";

interface p {
  onClose: () => void;
  isOpen: boolean;
  onFinish: (values: any) => void;
  setTrainingFacilitys: () => void;
  formRef: React.MutableRefObject<FormInstance<any>>;
}

const ModalAddNewTrainingFacility2 = ({
  onClose,
  onFinish,
  isOpen,
  formRef,
  setTrainingFacilitys,
}: p) => {
  return (
    <Modal
      title="Thêm mới cơ sở đào tạo"
      open={isOpen}
      onCancel={onClose}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Box px="16px" py="16px">
        <FormAddNewOrganization
          onFinish={onFinish}
          formRef={formRef}
          onClose={() => {
            onClose();
            setTrainingFacilitys();
          }}
        />
      </Box>
    </Modal>
  );
};

export default ModalAddNewTrainingFacility2;
