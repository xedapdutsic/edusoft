import { Box } from "@chakra-ui/react";
import { Form, Input } from "antd";
import React from "react";

const Organization = () => {
  return (
    <Box>
      <Form.Item
        labelAlign="left"
        label="Tên tổ chức"
        name="organizationName_2"
        rules={[{ required: true, message: "Vui lòng nhập tên tổ chức!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Mã tổ chức"
        name="organizationCode_2"
        rules={[{ required: true, message: "Vui lòng nhập mã tổ chức!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Số điện thoại"
        name="phoneNumber_2"
        rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Email"
        name="Email_2"
        rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Địa chỉ"
        name="address_2"
        rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Fax"
        name="Fax_2"
        rules={[{ required: true, message: "Vui lòng nhập Fax!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
    </Box>
  );
};

export default Organization;
