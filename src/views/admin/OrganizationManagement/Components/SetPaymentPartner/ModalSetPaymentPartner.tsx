import { Box, Flex, Text } from "@chakra-ui/react";
import { Button, Modal, Select } from "antd";
import MainButton from "components/button";
import { HSeparator } from "components/separator/Separator";
import React, { useEffect, useState } from "react";
import Cloes from "../../Icon/Cloes";
import ItemPartner from "./ItemPartner";

interface p {
  onClose: () => void;
  isOpen: boolean;
  handleSetVisible: () => void;
}

const ModalSetPaymentPartner = ({ onClose, isOpen, handleSetVisible }: p) => {
  const [isDisable, setIsDisable] = useState(false);
  const [listPartner, setListPartner] = useState<
    {
      name: string;
      value: string;
    }[]
  >([]);

  const handleRemove = (value: string) => {
    const isFind = listPartner.findIndex((e) => e.value === value);
    if (isFind >= 0) {
      setListPartner([
        ...listPartner.slice(0, isFind),
        ...listPartner.slice(isFind + 1),
      ]);
    }
  };

  const handleChange = (value: string) => {
    setListPartner([...listPartner, { name: value, value }]);
  };

  useEffect(() => {
    listPartner.length === 0 && setIsDisable(true);
  }, [listPartner]);

  return (
    <Modal
      title="Thiết lập đối tác trung gian thanh toán"
      open={isOpen}
      onCancel={onClose}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Box px="16px" py="16px">
        <Text
          color="#000000"
          opacity="0.5"
          fontSize="12px"
          fontStyle="italic"
          mb="4"
        >
          *Đối tác trung gian thanh toán sẽ được hiển thị trên cổng thanh toán
          AQ Bill Gateway. Nếu không có đối tác được chọn, hệ thống sẽ hiển thị
          toàn bộ đối tác của cổng thanh toán.
        </Text>

        <Select
          showSearch
          onChange={handleChange}
          className="custom-input w-full"
          placeholder="Chọn đối tác"
          filterOption={(input, option) =>
            (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
          }
          options={[
            { value: "VNPay", label: "VNPay" },
            { value: "MOMO", label: "MOMO" },
          ]}
        />

        <Box>
          {listPartner.map((e) => (
            <ItemPartner
              value={e.value}
              name={e.name}
              handleRemove={handleRemove}
              setIsDisable={setIsDisable}
            />
          ))}
        </Box>

        <Flex justifyContent="center" mt="10">
          <Button
            className="custom-button button-dash"
            onClick={onClose}
            style={{
              width: "95px",
            }}
          >
            Hủy
          </Button>
          <Box ml="2">
            <MainButton
              disabled={isDisable}
              text="Tiếp tục"
              onClick={() => {
                onClose();
                handleSetVisible();
              }}
            />
          </Box>
        </Flex>
      </Box>
    </Modal>
  );
};

export default ModalSetPaymentPartner;
