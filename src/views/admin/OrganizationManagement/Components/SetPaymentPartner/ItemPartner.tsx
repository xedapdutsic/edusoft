import { Box, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import { Input } from "antd";
import React, { useEffect, useState } from "react";
import { globalStyles } from "theme/styles";
import Accept from "../../Icon/Accept";
import Cloes from "../../Icon/Cloes";

interface P {
  name: string;
  value: string;
  handleRemove: (id: string) => void;
  setIsDisable: React.Dispatch<React.SetStateAction<boolean>>;
}

const ItemPartner = ({ name, value, handleRemove, setIsDisable }: P) => {
  const [defaulValue, setDefaulValue] = useState("");
  const [valueChange, setValueChange] = useState("");
  const [isShow, setIsShow] = useState(true);

  useEffect(() => {
    setIsShow(true);
    if (valueChange && defaulValue === valueChange) {
      setIsShow(false);
      setIsDisable(false);
    }
  }, [valueChange, defaulValue, setIsDisable]);

  return (
    <SimpleGrid
      key={value}
      border={"1px"}
      borderColor={"#B8EBFF"}
      borderStyle="dashed"
      borderRadius="20px"
      display="flex"
      alignItems="center"
      px="17px"
      // w={"75%"}
      columns={2}
      spacing={10}
      my="3"
      h="52px"
      justifyContent={"space-between"}
    >
      <Box>
        <Text color={globalStyles.colors.text[200]} pl="2">
          {name}
        </Text>
      </Box>
      <Flex alignItems={"center"}>
        <Text color={globalStyles.colors.text["4A4A4A"]} pr="2" w="120px">
          Merchant ID:
        </Text>
        <Input
          name={value}
          onChange={(e) => {
            setValueChange(e.target.value);
          }}
        />
      </Flex>
      <Flex alignItems={"center"}>
        {isShow && (
          <Box pr="2" onClick={() => setDefaulValue(valueChange)}>
            <Accept />
          </Box>
        )}
        <Box onClick={() => handleRemove(value)}>
          <Cloes />
        </Box>
      </Flex>
    </SimpleGrid>
  );
};

export default ItemPartner;
