import { Box } from "@chakra-ui/react";
import { Button, Form, FormInstance, Input, Select } from "antd";
import MainButton from "components/button";
import React from "react";

const { Option } = Select;
const HeaderSearch = () => {
  const formRef = React.useRef<FormInstance>(null);

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onReset = () => {
    formRef?.current?.resetFields();
  };

  return (
    <Box width={"100%"}>
      <Form onFinish={onFinish} ref={formRef} style={{ maxWidth: "100%" }}>
        <Box display="flex" flexWrap="wrap">
          <Box mr="2">
            <Form.Item name="username1" className="wrap-input">
              <Input placeholder="Tên tổ chức" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username2">
              <Input placeholder="Mã tổ chức" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username3">
              <Input placeholder="Số điện thoại" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username4">
              <Input placeholder="Địa chỉ" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="gender">
              <Select placeholder="Trạng thái" allowClear className="w-200">
                <Option value="male">male</Option>
                <Option value="female">female</Option>
                <Option value="other">other</Option>
              </Select>
            </Form.Item>
          </Box>
          <Box mr={2}>
            <Form.Item>
              <MainButton htmlType="submit" text="Tìm kiếm" />
            </Form.Item>
          </Box>

          <Form.Item>
            <Button
              className="custom-button button-danger"
              danger
              onClick={onReset}
            >
              Bỏ lọc
            </Button>
          </Form.Item>
        </Box>
      </Form>
    </Box>
  );
};

export default HeaderSearch;
