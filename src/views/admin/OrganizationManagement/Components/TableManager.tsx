import { Box, Button, Flex, Text, useDisclosure } from "@chakra-ui/react";
import { FormInstance } from "antd";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useState } from "react";
import ModalAddNewOrganization from "./AddNewOrganization/ModalAddNewOrganization";
import ModalAddNewTrainingFacility from "./AddNewTrainingFacility/ModalAddNewTrainingFacility";
import ModalOrganizationManagement from "./EditOrganization/ModalOrganizationManagement";
import { useHistory } from "react-router-dom";
import { OrganizationItem } from "service/OrganizationAdmin/Organization";

interface IProps {
  dataSource: Array<OrganizationItem & { key: string }>;
  totalRows: number;
  tableParams: {
    pagination?: TablePaginationConfig;
  };
  setDataSource: React.Dispatch<
    React.SetStateAction<
      (OrganizationItem & {
        key: string;
      })[]
    >
  >;
  setTableParams: React.Dispatch<
    React.SetStateAction<{
      pagination?: TablePaginationConfig;
    }>
  >;
}

const TableManager = ({
  dataSource,
  tableParams,
  setDataSource,
  setTableParams,
  totalRows,
}: IProps) => {
  const history = useHistory();
  const formRef = React.useRef<FormInstance>(null);
  const { isOpen, onClose } = useDisclosure();
  const [isVisibleModalAdd, setIsVisibleModalAdd] = useState(false);
  const [openTrainingFacility, setOpenTrainingFacility] = useState(false);

  const handleNavigate = (id: string) => {
    history.push(`/admin/organization-detail/${id}`);
  };

  const columns: ColumnsType<OrganizationItem> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Tên tổ chức",
      dataIndex: "orgName",
      key: "orgName",
      render: (text, data) => {
        return (
          <Text
            onClick={() => handleNavigate(data.orgCode)}
            cursor="pointer"
            style={{
              color: "#045993",
            }}
          >
            {text}
          </Text>
        );
      },
    },
    {
      title: "Mã tổ chức",
      dataIndex: "orgCode",
      key: "orgCode",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
    },
  ];

  const onReset = () => {
    setIsVisibleModalAdd(false);
    formRef?.current?.setFieldsValue({
      Email: "",
      endDate: "",
      address: "",
      phoneNumber: "",
      email_login: "",
      address_login: "",
      activationDate: "",
      fullName_login: "",
      username_login: "",
      organizationCode: "",
      organizationName: "",
      phoneNumber_login: "",
    });
  };

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: {totalRows} người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
          onClick={() => setIsVisibleModalAdd(true)}
        >
          Thêm mới
        </Button>
      </Flex>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
      {/* modal edit */}
      <ModalOrganizationManagement isOpen={isOpen} onClose={onClose} />

      {/* Thêm mới tổ chức */}
      <ModalAddNewOrganization
        isOpen={isVisibleModalAdd}
        onClose={() => setIsVisibleModalAdd(false)}
        formRef={formRef}
      />
      {/* Thêm mới cơ sở đào tạo */}
      <ModalAddNewTrainingFacility
        isOpen={openTrainingFacility}
        onClose={() => setOpenTrainingFacility(false)}
        onReset={onReset}
      />
    </Box>
  );
};

export default TableManager;
