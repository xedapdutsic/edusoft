import { Box } from "@chakra-ui/react";
import { DatePicker, Form, Input } from "antd";
import React from "react";
import { dateFormat } from "util/Constant";

const Organization = () => {
  return (
    <Box>
      <Form.Item
        labelAlign="left"
        label="Tên tổ chức"
        name="organizationName"
        rules={[{ required: false, message: "Vui lòng nhập tên tổ chức!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Mã tổ chức"
        name="organizationCode"
        rules={[{ required: true, message: "Vui lòng nhập mã tổ chức!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Số điện thoại"
        name="phoneNumber"
        rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Email"
        name="Email"
        rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Địa chỉ"
        name="address"
        rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
      >
        <Input className="custom-input w-330" />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Ngày kích hoạt"
        name="activationDate"
        rules={[{ required: true, message: "Vui lòng nhập ngày kích hoạt!" }]}
      >
        <DatePicker
          format={dateFormat}
          className="custom-input w-330"
          placeholder=""
        />
      </Form.Item>
      <Form.Item
        labelAlign="left"
        label="Ngày kết thúc"
        name="endDate"
        rules={[{ required: true, message: "Vui lòng nhập ngày kết thúc!" }]}
      >
        <DatePicker
          format={dateFormat}
          className="custom-input w-330"
          placeholder=""
        />
      </Form.Item>
    </Box>
  );
};

export default Organization;
