import { Box } from "@chakra-ui/react";
import { FormInstance, message, Modal } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import OrganizationAdminService from "service/OrganizationAdmin/OrganizationAdminService";
import { setLoadingApp } from "store/appSlice";
import { validResponse } from "util/API";
import { CODE_SUCCESS } from "util/Constant";
import Cloes from "../../Icon/Cloes";
import FormAddNewOrganization from "./FormAddNewOrganization";

interface p {
  onClose: () => void;
  isOpen: boolean;
  formRef: React.MutableRefObject<FormInstance<any>>;
}

const ModalAddNewOrganization = ({ onClose, isOpen, formRef }: p) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { CreateOrganization } = OrganizationAdminService;

  const onFinish = async (values: any) => {
    try {
      dispatch(setLoadingApp(true));
      const res = await CreateOrganization(values);
      if (res && validResponse(res) && res.data.code === CODE_SUCCESS) {
        localStorage.setItem("addOrganization", JSON.stringify(res.data.data));

        history.replace({
          pathname: `/admin/add-new-organization`,
          state: values,
        });
      } else {
        message.error("Có lỗi xảy ra !");
      }
    } catch (error) {
    } finally {
      dispatch(setLoadingApp(false));
    }
  };

  return (
    <Modal
      title="Thêm mới tổ chức"
      open={isOpen}
      onCancel={onClose}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Box px="16px" py="16px">
        {isOpen && (
          <FormAddNewOrganization
            onFinish={onFinish}
            onClose={onClose}
            formRef={formRef}
          />
        )}
      </Box>
    </Modal>
  );
};

export default ModalAddNewOrganization;
