import { Box, Flex } from "@chakra-ui/react";
import { Button, Form } from "antd";
import MainButton from "components/button";
import React, { FC } from "react";
import GeneralInformation from "./GeneralInformation";
import OperationStatus from "./OperationStatus";

interface IProps {
  onFinish: (values: any) => void;
  onClose: () => void;
}

const FormEditUser: FC<IProps> = ({ onFinish, onClose }: IProps) => {
  return (
    <Form name="basic" onFinish={onFinish} autoComplete="off">
      <Flex justifyContent="center" flexDirection={"column"}>
        {/* Thông tin chung */}
        <GeneralInformation />

        {/* Trạng thái hoạt động */}
        <OperationStatus />

        <Flex justifyContent="center">
          <Button
            htmlType="submit"
            className="custom-button button-dash"
            onClick={onClose}
            style={{
              width: "95px",
            }}
          >
            Hủy
          </Button>
          <Box ml="2">
            <MainButton htmlType="submit" text="Xác nhận" />
          </Box>
        </Flex>
      </Flex>
    </Form>
  );
};

export default FormEditUser;
