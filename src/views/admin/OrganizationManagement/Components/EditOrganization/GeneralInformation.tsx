import { Box, SimpleGrid, Text } from "@chakra-ui/react";
import { Form, Input } from "antd";
import React from "react";

const GeneralInformation = () => {
  return (
    <Box>
      <Text mb="4">Thông tin chung</Text>
      <SimpleGrid columns={3} spacing={3} spacingY="-1" mb="4">
        <Form.Item
          name="username"
          rules={[{ required: true, message: "Vui lòng nhập tên tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Tên tổ chức" />
        </Form.Item>
        <Form.Item
          name="username2"
          rules={[{ required: true, message: "Vui lòng nhập mã tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Mã tổ chức" />
        </Form.Item>
        <Form.Item
          name="username3"
          rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
        >
          <Input className="custom-input" placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="username4"
          rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
        >
          <Input className="custom-input" placeholder="Số điện thoại" />
        </Form.Item>
        <Form.Item
          name="username4"
          rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
        >
          <Input className="custom-input" placeholder="Địa chỉ" />
        </Form.Item>
      </SimpleGrid>
    </Box>
  );
};

export default GeneralInformation;
