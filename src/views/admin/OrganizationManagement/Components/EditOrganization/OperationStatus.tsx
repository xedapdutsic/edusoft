import { Box, Grid, GridItem, Text } from "@chakra-ui/react";
import { DatePicker } from "antd";
import { Form, Select } from "antd";
import React from "react";
import { dateFormat } from "util/Constant";
const { RangePicker } = DatePicker;

const { Option } = Select;

const OperationStatus = () => {
  return (
    <Box>
      <Text mb="4">Trạng thái hoạt động</Text>
      <Grid templateColumns="repeat(4, 1fr)" gap={3}>
        <GridItem colSpan={1}>
          <Form.Item
            name="gender"
            rules={[{ required: true, message: "Vui lòng nhập quyền!" }]}
          >
            <Select placeholder="Trạng thái" allowClear className="w-custom-1">
              <Option value="male">male</Option>
              <Option value="female">female</Option>
              <Option value="other">other</Option>
            </Select>
          </Form.Item>
        </GridItem>
        <GridItem colStart={2} colEnd={5}>
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Vui lòng nhập thời gian!" }]}
          >
            <RangePicker format={dateFormat} className="custom-input w-full" />
          </Form.Item>
        </GridItem>
      </Grid>
    </Box>
  );
};

export default OperationStatus;
