import { Box } from "@chakra-ui/react";
import { Modal } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import Cloes from "../../Icon/Cloes";
import FormEditOrganization from "./FormEditOrganization";

interface p {
  onClose: () => void;
  isOpen: boolean;
}

const ModalOrganizationManagement = ({ onClose, isOpen }: p) => {
  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  return (
    <Modal
      style={{ top: "20%" }}
      title="Chi tiết tổ chức"
      open={isOpen}
      onCancel={onClose}
      width={1000}
      footer={<></>}
      closeIcon={
        <>
          <Cloes />
        </>
      }
    >
      <HSeparator />
      <Box px="16px" py="16px">
        <FormEditOrganization onFinish={onFinish} onClose={onClose} />
      </Box>
    </Modal>
  );
};

export default ModalOrganizationManagement;
