import { Box } from "@chakra-ui/react";
// import { HSeparator } from "components/separator/Separator";
// import HeaderSearch from "./Components/HeaderSearch";
import { globalStyles } from "theme/styles";
import AgTable from "Aggrid/AgTable";

const OrganizationManagement = () => {
  const columnDefs = [
    {
      //field: 'stt',
      headerName: "STT",
      // valueFormatter: (params: any) => `${params.node.rowIndex + 1}`,
      maxWidth: 70,
      suppressMenu: true,
      sortable: false,
    },
    {
      field: "orgName",
      headerName: "Tên tổ chức",
      width: 200,
      filter: "agTextColumnFilter",
      type: "textFilter",
      // cellRenderer: (e: any) => {
      //   return (
      //     <span>{`${e.value}`}</span>
      //   );
      // },
    },
    {
      field: "orgCode",
      headerName: "Mã tổ chức",
      minWidth: 150,
      filter: "agTextColumnFilter",
      type: "textFilter",
    },
    {
      field: "phone",
      headerName: "Số điện thoại",
      minWidth: 150,
      suppressMenu: true,
      filterBox: true,
      type: "numberFilter",
    },

    {
      field: "address",
      headerName: "Địa chỉ",
      width: 200,
      filter: "agTextColumnFilter",
      type: "textFilter",
    },
    {
      field: "status",
      headerName: "Trạng thái",
      width: 150,
      // cellRenderer: (e: any) => <span>{`${e.value}`}</span>,
      filter: "statusFilter",
      // filterParams: {
      //   listStatus: [
      //     {
      //       title: "ACTIVE",
      //       value: "ACTIVE",
      //     },

      //     {
      //       title: "INACTIVE",
      //       value: "INACTIVE",
      //     },
      //     {
      //       title: "APPROVAL_PENDING",
      //       value: "APPROVAL_PENDING",
      //     },
      //   ],
      // },
    },
  ];
  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
    >
      {/* <HeaderSearch /> */}
      {/* <HSeparator /> */}
      {/* <TableManager
        totalRows={totalRows}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      /> */}
      <AgTable url="/organization" columnDefs={columnDefs} />
    </Box>
  );
};

export default OrganizationManagement;
