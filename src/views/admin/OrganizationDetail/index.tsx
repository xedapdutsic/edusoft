import { Box } from "@chakra-ui/react";
import { Button, message } from "antd";
import React, { useEffect, useMemo, useState } from "react";
import { useDispatch } from "react-redux";
import OrganizationAdminService from "service/OrganizationAdmin/OrganizationAdminService";
import {
  setActiveRoute,
  setBrandText,
  setBreadcrumb,
  setDetailOrganization,
  setLoadingApp,
} from "store/appSlice";
import { globalStyles } from "theme/styles";
import { validResponse } from "util/API";
import { ADMIN, CODE_SUCCESS } from "util/Constant";
import OrganizationInformation from "./Components/OrganizationInformation";
import TrainingFacilitiesView from "views/admin/AddNewOrganization/Components/TrainingFacilitiesView";
import "./style.css";
import { LocalStorageType } from "types/general";

const OrganizationDetail = () => {
  const [activeBtn, setAcitiveBtn] = useState(1);
  const ID = useMemo(() => window.location.hash.slice(28), []);
  const dispatch = useDispatch();

  // /add-new-organization

  const handleGetOrganztionDetail = async () => {
    try {
      dispatch(setLoadingApp(true));
      const res = await OrganizationAdminService.OrganizationDetail(ID);
      if (res && validResponse(res) && res.data.code === CODE_SUCCESS) {
        dispatch(setDetailOrganization(res.data.data));
      } else {
        message.error("Có lỗi xảy ra !");
      }
    } catch (error) {
      message.error("Có lỗi xảy ra !");
    } finally {
      dispatch(setLoadingApp(false));
    }
  };

  useEffect(() => {
    handleGetOrganztionDetail();
  }, []);

  useEffect(() => {
    dispatch(setActiveRoute("/organization-management"));
    return () => {
      dispatch(setActiveRoute(""));
      dispatch(setBreadcrumb([]));
      dispatch(setBrandText(undefined));
    };
  }, [dispatch]);

  useEffect(() => {
    const localValue: LocalStorageType | undefined = JSON.parse(
      localStorage.getItem("addOrganization")
    );

    localValue &&
      dispatch(
        setBrandText({
          name: "Quản lý tổ chức",
          link: `${ADMIN}/organization-detail/${localValue.orgCode}`,
        })
      );

    if (localValue && activeBtn === 2) {
      const list = [
        {
          name: localValue.orgName,
          link: `${ADMIN}/organization-detail/${localValue.orgCode}`,
        },
        {
          name: "Cơ sở đào tạo",
          link: `${ADMIN}/organization-detail/${localValue.orgCode}`,
        },
      ];
      dispatch(setBreadcrumb(list));
    }
  }, [activeBtn, dispatch]);

  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
      borderTopLeftRadius={0}
      position="relative"
    >
      <Box className="tabs-panel">
        <Button
          onClick={() => setAcitiveBtn(1)}
          className={`organization-information ${
            activeBtn === 1 ? "active" : ""
          }`}
        >
          Thông tin tổ chức
        </Button>
        <Button
          onClick={() => setAcitiveBtn(2)}
          className={`training-facilities ${activeBtn === 2 ? "active" : ""}`}
        >
          Cơ sở đào tạo
        </Button>
      </Box>
      <Box>
        {activeBtn === 1 && <OrganizationInformation />}
        {activeBtn === 2 && <TrainingFacilitiesView />}
      </Box>
    </Box>
  );
};

export default OrganizationDetail;
