import { Box, Flex } from "@chakra-ui/react";
import { Form, Input, Select, DatePicker, FormInstance } from "antd";
import MainButton from "components/button";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "store";
import { setBreadcrumb, Type_Initstate } from "store/appSlice";
import { ADMIN, dateFormat } from "util/Constant";
const { Option } = Select;

interface IProps {
  onFinish: (values: any) => void;
}

const FormOrganization = ({ onFinish }: IProps) => {
  const dispatch = useDispatch();
  const appState = useSelector<RootState, Type_Initstate>((state) => state.app);
  const { detailOrganization } = appState;
  const formRef = React.useRef<FormInstance>(null);

  useEffect(() => {
    if (detailOrganization) {
      formRef?.current?.setFieldsValue({
        orgName: detailOrganization.orgName,
        orgCode: detailOrganization.orgCode,
        address: detailOrganization.address,
        email: detailOrganization.email,
        phone: detailOrganization.phone,
        endTime: moment(detailOrganization.endTime),
        startTime: moment(detailOrganization.startTime),
        status: detailOrganization.status,
      });
      const list = {
        name: detailOrganization.orgName,
        link: `${ADMIN}/organization-detail/${detailOrganization.orgCode}`,
      };
      dispatch(setBreadcrumb([list]));
    }
  }, [detailOrganization, dispatch]);

  return (
    <Form
      name="basic"
      onFinish={onFinish}
      autoComplete="off"
      labelCol={{ span: 4 }}
      ref={formRef}
      // wrapperCol={{ span: 16 }}
    >
      <Box w="80%" maxW="755px">
        <Form.Item
          name="orgName"
          label="Tên tổ chức"
          labelAlign="left"
          rules={[{ required: true, message: "Vui lòng nhập tên tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Tên tổ chức" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Mã tổ chức"
          name="orgCode"
          rules={[{ required: true, message: "Vui lòng nhập mã tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Mã tổ chức" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Email"
          name="email"
          rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
        >
          <Input className="custom-input" placeholder="Email" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Số điện thoại"
          name="phone"
          rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
        >
          <Input className="custom-input" placeholder="Số điện thoại" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Địa chỉ"
          name="address"
          rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
        >
          <Input className="custom-input" placeholder="Địa chỉ" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Trạng thái"
          name="status"
          rules={[{ required: true, message: "Vui lòng trạng thái!" }]}
        >
          <Select placeholder="Trạng thái" allowClear className="w-full">
            <Option value="1">1</Option>
            <Option value="2">2</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="startTime"
          labelAlign="left"
          label="Ngày kích hoạt"
          rules={[{ required: true, message: "Vui lòng chọn thời gian!" }]}
        >
          <DatePicker format={dateFormat} className="custom-input w-full" />
        </Form.Item>
        <Form.Item
          name="endTime"
          labelAlign="left"
          label="Ngày kết thúc"
          rules={[{ required: true, message: "Vui lòng chọn thời gian!" }]}
        >
          <DatePicker format={dateFormat} className="custom-input w-full" />
        </Form.Item>
        <Flex justifyContent="flex-end">
          <MainButton
            htmlType="submit"
            // style={{
            //   width: "95px",
            // }}
            text="Cập nhật"
          />
        </Flex>
      </Box>
    </Form>
  );
};

export default FormOrganization;
