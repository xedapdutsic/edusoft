import { Text } from "@chakra-ui/react";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import FormOrganization from "./FormOrganization";

const OrganizationInformation = () => {
  const onFinish = (values: any) => {
    console.log(values);
  };

  return (
    <div>
      <Text pb="20px">Thông tin chung</Text>
      <FormOrganization onFinish={onFinish} />
      <HSeparator mb="20px" mt={5} />
    </div>
  );
};

export default OrganizationInformation;
