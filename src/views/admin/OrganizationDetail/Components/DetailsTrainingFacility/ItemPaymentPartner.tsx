import { Box, Flex, Text } from "@chakra-ui/react";
import { Button, Input } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";

// const fakeData = [
//   { title: "Ứng dụng thanh toán hỗ trợ VNPAYQR", value: "1" },
//   { title: "Thẻ thanh toán quốc tế", value: "2" },
//   { title: "Thẻ nội địa và tài khoản ngân hàng", value: "3" },
//   { title: "Ví điện tử VNPAY", value: "4" },
// ];

const ItemPaymentPartner = () => {
  // const [value, setValue] = useState(1);

  // const onChange = (e: RadioChangeEvent) => {
  //   console.log("radio checked", e.target.value);
  //   setValue(e.target.value);
  // };

  return (
    <Box
      maxW="sm"
      borderWidth="1px"
      borderRadius="16px"
      w="300px"
      boxShadow={"0px 2px 94px rgba(114, 132, 155, 0.1)"}
    >
      <Flex justifyContent="center" alignItems="center" py="6">
        <img src={require("assets/img/Logo/VNPAY.png")} alt="" />
      </Flex>
      <HSeparator />
      <Box py="6" px="4">
        <Flex alignItems="center" justifyContent="space-between" mb="3">
          <Text w="130px" fontSize="14px">
            Merchant ID
          </Text>{" "}
          <Input className="custom-input" />
        </Flex>
        <Flex alignItems="center" justifyContent="space-between">
          <Text w="130px" fontSize="14px" textAlign="left">
            tID
          </Text>{" "}
          <Input className="custom-input" />
        </Flex>

        <Flex p="6" pb="0" justifyContent={"space-between"}>
          <Button className="custom-button btn-FF4B4B">Xóa</Button>
          <Button className="custom-button btn-F3C262">Sửa</Button>
        </Flex>
      </Box>
    </Box>
  );
};

export default ItemPaymentPartner;
