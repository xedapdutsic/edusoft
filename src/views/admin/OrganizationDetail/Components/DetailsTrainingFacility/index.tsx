import React from "react";
import HeaderSearch from "./HeaderSearch";
import PaymentPartner from "./PaymentPartner";

const DetailsTrainingFacility = () => {
  return (
    <div>
      <HeaderSearch />
      <PaymentPartner />
    </div>
  );
};

export default DetailsTrainingFacility;
