import { Button, Flex, Text } from "@chakra-ui/react";
import React from "react";
import ItemPaymentPartner from "./ItemPaymentPartner";

const PaymentPartner = () => {
  return (
    <div>
      <Flex justifyContent="space-between" alignItems="center">
        <Text fontSize="15px" fontWeight="500" my="6">
          Đối tác thanh toán
        </Text>

        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
        >
          Thêm mới
        </Button>
      </Flex>

      <ItemPaymentPartner />
    </div>
  );
};

export default PaymentPartner;
