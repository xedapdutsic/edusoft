import { Box } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { globalStyles } from "theme/styles";
import OrganizationInformation from "./Components/OrganizationInformation";
import TabsPanel from "components/TabsPanel";
import "./style.css";
import { useDispatch } from "react-redux";
import { LocalStorageType } from "types/general";
import { setBreadcrumb } from "store/appSlice";
import { ADMIN } from "util/Constant";

const AddNewOrganization = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const localValue: LocalStorageType | undefined = JSON.parse(
      localStorage.getItem("addOrganization")
    );

    if (localValue) {
      const list = {
        name: localValue.orgName,
        link: `${ADMIN}/add-new-organization`,
      };
      dispatch(setBreadcrumb([list]));
    }
  }, [dispatch]);

  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
      borderTopLeftRadius={0}
      position="relative"
    >
      <TabsPanel activeBtn={1} />
      <Box>
        <OrganizationInformation />
      </Box>
    </Box>
  );
};

export default AddNewOrganization;
