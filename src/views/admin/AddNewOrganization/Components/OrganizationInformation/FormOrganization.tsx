import { Box, Flex } from "@chakra-ui/react";
import { Form, Input, Select, DatePicker, FormInstance } from "antd";
import MainButton from "components/button";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { LocalStorageType, OrganizationInput } from "types/general";
import { dateFormat } from "util/Constant";
const { Option } = Select;

interface IProps {
  onFinish: (values: any) => void;
}

const FormOrganization = ({ onFinish }: IProps) => {
  const [initOrg, setInitOrg] = useState<LocalStorageType | undefined>(
    undefined
  );
  const [initValue, setInitinitValue] = useState<OrganizationInput | undefined>(
    undefined
  );
  const formRef = React.useRef<FormInstance>(null);

  useEffect(() => {
    const localValue: LocalStorageType | undefined = JSON.parse(
      localStorage.getItem("addOrganization")
    );
    const initCreateOrganization: OrganizationInput | undefined = JSON.parse(
      localStorage.getItem("createOrganization")
    );

    if (localValue) {
      setInitOrg(localValue);
    }

    if (initCreateOrganization) {
      setInitinitValue(initCreateOrganization);
    }
  }, []);

  useEffect(() => {
    if (initOrg) {
      formRef?.current?.setFieldsValue({
        orgName: initOrg.orgName,
        orgCode: initOrg.orgCode,
      });
    }
    if (initValue) {
      formRef?.current?.setFieldsValue({
        orgName: initValue.orgName,
        orgCode: initValue.orgCode,
        address: initValue.address,
        email: initValue.email,
        phone: initValue.phone,
        endTime: moment(initValue.endTime),
        startTime: moment(initValue.startTime),
        status: initValue.status,
      });
    }
  }, [initOrg, initValue]);

  return (
    <Form
      name="basic"
      onFinish={onFinish}
      autoComplete="off"
      labelCol={{ span: 4 }}
      ref={formRef}
    >
      <Box w="80%" maxW="755px">
        <Form.Item
          name="orgName"
          label="Tên tổ chức"
          labelAlign="left"
          className="text-bold"
          initialValue={initOrg?.orgName}
          rules={[{ required: true, message: "Vui lòng nhập tên tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Tên tổ chức" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Mã tổ chức"
          name="orgCode"
          className="text-bold"
          initialValue={initOrg?.orgCode}
          rules={[{ required: true, message: "Vui lòng nhập mã tổ chức!" }]}
        >
          <Input className="custom-input " placeholder="Mã tổ chức" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Email"
          name="email"
          className="text-bold"
          initialValue={initValue?.email}
          rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
        >
          <Input className="custom-input" placeholder="Email" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Số điện thoại"
          className="text-bold"
          name="phone"
          initialValue={initValue?.phone}
          rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
        >
          <Input className="custom-input" placeholder="Số điện thoại" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Địa chỉ"
          className="text-bold"
          name="address"
          initialValue={initValue?.address}
          rules={[{ required: true, message: "Vui lòng nhập địa chỉ!" }]}
        >
          <Input className="custom-input" placeholder="Địa chỉ" />
        </Form.Item>
        <Form.Item
          labelAlign="left"
          label="Trạng thái"
          className="text-bold"
          name="status"
          // initialValue={initValue?.status}
          rules={[{ required: true, message: "Vui lòng trạng thái!" }]}
        >
          <Select allowClear className="w-full">
            <Option value="1">1</Option>
            <Option value="2">2</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="startTime"
          labelAlign="left"
          className="text-bold"
          label="Ngày kích hoạt"
          // initialValue={initValue?.startTime}
          rules={[{ required: true, message: "Vui lòng chọn thời gian!" }]}
        >
          <DatePicker format={dateFormat} className="custom-input w-full" />
        </Form.Item>
        <Form.Item
          name="endTime"
          labelAlign="left"
          label="Ngày kết thúc"
          className="text-bold"
          // initialValue={initValue?.endTime}
          rules={[{ required: true, message: "Vui lòng chọn thời gian!" }]}
        >
          <DatePicker format={dateFormat} className="custom-input w-full" />
        </Form.Item>
        <Flex justifyContent="flex-end">
          <MainButton
            htmlType="submit"
            // style={{
            //   width: "95px",
            // }}
            text="Cập nhật"
          />
        </Flex>
      </Box>
    </Form>
  );
};

export default FormOrganization;
