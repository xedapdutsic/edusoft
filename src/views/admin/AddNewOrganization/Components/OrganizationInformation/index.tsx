import { Text } from "@chakra-ui/react";
import { HSeparator } from "components/separator/Separator";
import { useDispatch } from "react-redux";
import { globalStyles } from "theme/styles";
import { LocalStorageType, OrganizationInput } from "types/general";
import { CODE_SUCCESS } from "util/Constant";
import FormOrganization from "./FormOrganization";
import OrganizationAdminService from "service/OrganizationAdmin/OrganizationAdminService";
import { validResponse } from "util/API";
import { message } from "antd";
import { setLoadingApp } from "store/appSlice";

const OrganizationInformation = () => {
  const dispatch = useDispatch();

  const { OrganizationUpdate } = OrganizationAdminService;

  const onFinish = async (values: OrganizationInput) => {
    try {
      const localValue: LocalStorageType | undefined = JSON.parse(
        localStorage.getItem("addOrganization")
      );
      dispatch(setLoadingApp(true));
      const res = await OrganizationUpdate({ ...values, _id: localValue._id });
      if (
        res &&
        validResponse(res) &&
        res.data.code === CODE_SUCCESS &&
        !res.data.data.statusCode
      ) {
        message.success("Cập nhật thành công");
      } else {
        message.error("Có lỗi xảy ra !");
      }
    } catch (error) {
    } finally {
      dispatch(setLoadingApp(false));
    }
  };

  return (
    <div>
      <Text
        color={globalStyles.colors.text[202027]}
        pb="20px"
        fontWeight="600"
        fontSize="15px"
      >
        Thông tin chung
      </Text>
      <FormOrganization onFinish={onFinish} />
      <HSeparator mb="20px" mt={5} />
    </div>
  );
};

export default OrganizationInformation;
