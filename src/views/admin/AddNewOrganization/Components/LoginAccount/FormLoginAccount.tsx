import { Box, Flex } from "@chakra-ui/react";
import { Form, Input } from "antd";
import MainButton from "components/button";

interface IProps {
  onFinish: (values: any) => void;
}

const FormLoginAccount = ({ onFinish }: IProps) => {
  return (
    <Form
      name="basic"
      onFinish={onFinish}
      autoComplete="off"
      labelCol={{ span: 4 }}
    >
      <Box w="80%" maxW="755px">
        <Form.Item
          name="username"
          label="Username"
          labelAlign="left"
          className="text-bold"
          rules={[{ required: true, message: "Vui lòng nhập username!" }]}
        >
          <Input className="custom-input " placeholder="username" />
        </Form.Item>
        <Form.Item
          name="fullName"
          label="Họ và tên"
          labelAlign="left"
          className="text-bold"
          rules={[{ required: true, message: "Vui lòng nhập họ và tên!" }]}
        >
          <Input className="custom-input " placeholder="Họ và tên" />
        </Form.Item>
        <Form.Item
          name="emailAdmin"
          label="Email"
          labelAlign="left"
          className="text-bold"
          rules={[{ required: true, message: "Vui lòng nhập Email!" }]}
        >
          <Input className="custom-input " placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="phoneAdmin"
          label="Số điện thoại"
          labelAlign="left"
          className="text-bold"
          rules={[{ required: true, message: "Vui lòng nhập số điện thoại!" }]}
        >
          <Input className="custom-input " placeholder="Số điện thoại" />
        </Form.Item>
        <Form.Item
          name="addressAdmin"
          label="Địa chỉ"
          labelAlign="left"
          className="text-bold"
          rules={[{ required: true, message: "Vui lòng nhập Địa chỉ!" }]}
        >
          <Input className="custom-input " placeholder="Địa chỉ" />
        </Form.Item>

        <Flex justifyContent="flex-end">
          <MainButton htmlType="submit" text="Thiết lập" />
        </Flex>
      </Box>
    </Form>
  );
};

export default FormLoginAccount;
