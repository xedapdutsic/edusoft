import { Box } from "@chakra-ui/react";
import { Button, Form, FormInstance, Input } from "antd";
import MainButton from "components/button";
import { HSeparator } from "components/separator/Separator";
import React from "react";

const HeaderSearch = () => {
  const formRef = React.useRef<FormInstance>(null);

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onReset = () => {
    formRef?.current?.resetFields();
  };

  return (
    <Box width={"100%"}>
      <Form onFinish={onFinish} ref={formRef} style={{ maxWidth: "100%" }}>
        <Box display="flex" flexWrap="wrap">
          <Box mr="2">
            <Form.Item name="username1" className="wrap-input">
              <Input placeholder="Tên cơ sở" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username2">
              <Input placeholder="Mã cơ sở" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username3">
              <Input placeholder="Số điện thoại" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username4">
              <Input placeholder="Email" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username5">
              <Input placeholder="Fax" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username6">
              <Input placeholder="Chi nhánh" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username7">
              <Input placeholder="Địa chỉ" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr={2}>
            <Form.Item>
              <MainButton htmlType="submit" text="Tìm kiếm" />
            </Form.Item>
          </Box>

          <Form.Item>
            <Button
              className="custom-button button-danger"
              danger
              onClick={onReset}
            >
              Bỏ lọc
            </Button>
          </Form.Item>
        </Box>
      </Form>
      <HSeparator />
    </Box>
  );
};

export default HeaderSearch;
