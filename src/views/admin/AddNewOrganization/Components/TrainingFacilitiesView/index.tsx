import React from "react";
import HeaderSearch from "./HeaderSearch";
import TableManager from "./TableManager";

const TrainingFacilities = () => {
  return (
    <div>
      <HeaderSearch />
      <TableManager />
    </div>
  );
};

export default TrainingFacilities;
