import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import OrganizationAdminService from "service/OrganizationAdmin/OrganizationAdminService";
import { setLoadingApp } from "store/appSlice";
import { LocalStorageType } from "types/general";
import { validResponse } from "util/API";
import { CODE_SUCCESS } from "util/Constant";

interface DataType {
  key: string;
  stt: string;
  organizationName: string;
  organizationCode: string;
  address: string;
  email: string;
  Fax: string;
  branch: string;
}

const pageSize = 10;

const TableManager = () => {
  const dispatch = useDispatch();
  const { GetSchool } = OrganizationAdminService;
  const [dataSource, setDataSource] = useState<Array<any & { key: string }>>(
    []
  );
  const [current, setCurrent] = useState(1);

  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  const handleAddKey = (data: Array<any>) => {
    let arr: Array<any & { key: string }> = [];
    arr = data.map((e) => ({ ...e, key: e._id }));
    return arr;
  };

  const handleGetAllOrganization = async (currentPage: number) => {
    try {
      const localValue: LocalStorageType | undefined = JSON.parse(
        localStorage.getItem("addOrganization")
      );
      dispatch(setLoadingApp(true));
      // let query = ``;
      let query = `?limit=${pageSize}&offset=${(currentPage - 1) * pageSize}`;

      const res = await GetSchool(localValue.orgCode, query);
      if (res && validResponse(res) && res.data.code === CODE_SUCCESS) {
        setDataSource(handleAddKey(res.data.data.records));
        setTableParams({
          pagination: {
            current,
            pageSize,
            total: res.data.data.pagination.totalRows,
          },
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(setLoadingApp(false));
    }
  };

  useEffect(() => {
    handleGetAllOrganization(current);
    // eslint-disable-next-line
  }, [current]);

  useEffect(() => {
    setCurrent(tableParams.pagination.current);
  }, [tableParams]);

  useEffect(() => {
    handleGetAllOrganization(current);
    // eslint-disable-next-line
  }, []);

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Tên cơ sở",
      dataIndex: "orgName",
      key: "orgName",
      render: (text, data) => {
        return (
          <Text
            // onClick={() => setIsShowDetail(true)}
            cursor="pointer"
            style={{
              color: "#045993",
            }}
          >
            {text}
          </Text>
        );
      },
    },
    {
      title: "Mã cơ sở",
      dataIndex: "orgCode",
      key: "orgCode",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Fax",
      dataIndex: "fax",
      key: "fax",
    },
    {
      title: "Chi nhánh",
      dataIndex: "branch",
      key: "branch",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
  ];

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: 50 người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
          // onClick={() => setIsVisibleModalAdd(true)}
        >
          Thêm mới
        </Button>
      </Flex>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
    </Box>
  );
};

export default TableManager;
