import { Box } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { globalStyles } from "theme/styles";
import TabsPanel from "components/TabsPanel";
import { useDispatch } from "react-redux";
import { LocalStorageType } from "types/general";
import { setBreadcrumb, setLoadingApp } from "store/appSlice";
import { ADMIN, CODE_SUCCESS } from "util/Constant";
import FormLoginAccount from "./Components/LoginAccount/FormLoginAccount";
import { HSeparator } from "components/separator/Separator";
import OrganizationAdminService from "service/OrganizationAdmin/OrganizationAdminService";
import { validResponse } from "util/API";
import { message } from "antd";

const LoginAccount = () => {
  const dispatch = useDispatch();
  const { OrganizationUpdate } = OrganizationAdminService;

  const onFinish = async (values: any) => {
    try {
      dispatch(setLoadingApp(true));
      const localValue: LocalStorageType | undefined = JSON.parse(
        localStorage.getItem("addOrganization")
      );
      const newData = {
        ...values,
        _id: localValue._id,
      };

      const res = await OrganizationUpdate(newData);
      if (
        res &&
        validResponse(res) &&
        res.data.code === CODE_SUCCESS &&
        !res.data.data.statusCode
      ) {
        message.success("chỉnh sửa tài khoản thành công");
        // history.push(`${ADMIN}/organization-management`);
      } else {
        message.error("Có lỗi xảy ra !");
      }
    } catch (error) {
      message.error("Có lỗi xảy ra !");
    } finally {
      dispatch(setLoadingApp(false));
    }
  };

  useEffect(() => {
    const localValue: LocalStorageType | undefined = JSON.parse(
      localStorage.getItem("addOrganization")
    );

    if (localValue) {
      const list = [
        {
          name: localValue.orgName,
          link: `${ADMIN}/add-new-organization`,
        },
        {
          name: "Tài khoản đăng nhập",
          link: `${ADMIN}/training-facilities`,
        },
      ];
      dispatch(setBreadcrumb(list));
    }
  }, [dispatch]);

  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
      borderTopLeftRadius={0}
      position="relative"
    >
      <TabsPanel activeBtn={2} />
      <Box>
        <FormLoginAccount onFinish={onFinish} />
        <HSeparator mb="20px" mt={5} />
      </Box>
    </Box>
  );
};

export default LoginAccount;
