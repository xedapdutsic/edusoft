import { Box } from "@chakra-ui/react";
import { Button, Form, Input, DatePicker } from "antd";
import MainButton from "components/button";
import React from "react";
import { dateFormat } from "util/Constant";
const HeaderSearch = () => {
  const { RangePicker } = DatePicker;
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };
  return (
    <Box width={"100%"}>
      <Form onFinish={onFinish} form={form} style={{ maxWidth: "100%" }}>
        <Box display="flex" flexWrap="wrap">
          <Box mr="2">
            <Form.Item name="username1" className="wrap-input">
              <Input placeholder="Username" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username">
              <Input placeholder="Người thực hiện" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="gender">
              <RangePicker
                format={dateFormat}
                className="custom-input w-full"
              />
            </Form.Item>
          </Box>
          <Box mr={2}>
            <Form.Item>
              <MainButton htmlType="submit" text="Tìm kiếm" />
            </Form.Item>
          </Box>

          <Form.Item>
            <Button
              htmlType="submit"
              className="custom-button button-danger"
              danger
            >
              Bỏ lọc
            </Button>
          </Form.Item>
        </Box>
      </Form>
    </Box>
  );
};

export default HeaderSearch;
