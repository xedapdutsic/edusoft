// Chakra imports
import { Box } from "@chakra-ui/react";
import { globalStyles } from "theme/styles";

export default function DashboardCSDT() {
  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
    >
      <button>Dashboard</button>
    </Box>
  );
}
