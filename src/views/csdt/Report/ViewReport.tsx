import { Button, Flex, Text } from "@chakra-ui/react";
import { Form, Input, DatePicker } from "antd";
import { HSeparator } from "components/separator/Separator";
import React from "react";
import { globalStyles } from "theme/styles";
import { dateFormat } from "util/Constant";

const { RangePicker } = DatePicker;

const ViewReport = () => {
  return (
    <>
      <Text
        color={globalStyles.colors.text[202027]}
        fontWeight="500"
        fontSize="15px"
        mb="4"
      >
        Chọn mẫu báo cáo
      </Text>

      <Form name="report">
        <Form.Item name="report-form">
          <Input className="custom-input w-full" />
        </Form.Item>
        <Form.Item name="date">
          <RangePicker format={dateFormat} className="custom-input w-full" />
        </Form.Item>

        <HSeparator mb="4" />

        <Flex justifyContent="space-between" alignItems="center">
          <Text
            color={globalStyles.colors.text[202027]}
            fontWeight="500"
            fontSize="15px"
            mb="4"
          >
            Xem trước
          </Text>

          <Button
            colorScheme="whatsapp"
            backgroundColor={"#5FAD67"}
            borderRadius="20px"
          >
            <i
              className="fa-solid fa-download "
              style={{
                color: "#fff",
              }}
            />
            <Text ml="2" color={"#fff"}>
              Excel
            </Text>
          </Button>
        </Flex>
      </Form>
    </>
  );
};

export default ViewReport;
