import { Box } from "@chakra-ui/react";
import React from "react";
import { globalStyles } from "theme/styles";
import HeaderSearch from "./Components/HeaderSearch";
import TableManager from "./Components/TableManager";

const OrganizationalUserManagement = () => {
  return (
    <Box
      p={{ base: "20px", md: "30px" }}
      minH="calc(100vh - 140px)"
      background={globalStyles.colors.bg_color[200]}
      borderRadius="16px"
    >
      <HeaderSearch />
      <TableManager />
    </Box>
  );
};

export default OrganizationalUserManagement;
