import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

interface DataType {
  key: string;
  stt: string;
  fullName: string;
  personnelCode: string;
  email: string;
  sdt: string;
  permission: string;
}

const current = 1;
const pageSize = 2;

const TableManager = () => {
  const history = useHistory();
  const [dataSource, setDataSource] = useState<DataType[]>([]);
  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  const handleNavigate = (id: string) => {
    history.push(`/admin/organization-detail/${id}`);
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Họ và tên",
      dataIndex: "fullName",
      key: "fullName",
      render: (text, data) => {
        return (
          <Text
            onClick={() => handleNavigate(data.fullName)}
            cursor="pointer"
            style={{
              color: "#045993",
            }}
          >
            {text}
          </Text>
        );
      },
    },
    {
      title: "Mã nhân sự",
      dataIndex: "personnelCode",
      key: "personnelCode",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "SĐT",
      dataIndex: "sdt",
      key: "sdt",
    },
    {
      title: "Quyền",
      dataIndex: "permission",
      key: "permission",
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      fullName: "Nguyễn Tuấn Anh",
      personnelCode: "235ASZCFF",
      email: "tuanhanh@gmail.com",
      sdt: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "1",
    },
    {
      key: "2",
      fullName: "Nguyễn Tuấn Anh",
      personnelCode: "235ASZCFF",
      email: "tuanhanh@gmail.com",
      sdt: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "2",
    },
    {
      key: "3",
      fullName: "Nguyễn Tuấn Anh",
      personnelCode: "235ASZCFF",
      email: "tuanhanh@gmail.com",
      sdt: "0912345678",
      permission: "Quản lý hệ thống",
      stt: "3",
    },
  ];

  useEffect(() => {
    setDataSource(data);
  }, []);

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: 50 người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
        >
          Thêm mới
        </Button>
      </Flex>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
    </Box>
  );
};

export default TableManager;
