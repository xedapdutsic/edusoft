import { Box, Text } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";

interface DataType {
  key: string;
  stt: string;
  username: string;
  executor: string;
  operation: string;
  executionTime: string;
}

const current = 1;
const pageSize = 2;

const TableManager = () => {
  // const history = useHistory();

  const [dataSource, setDataSource] = useState<DataType[]>([]);
  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  // const handleNavigate = (id: string) => {
  //   history.push(`/admin/user-detail/${id}`);
  // };

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
      render: (text) => (
        <Text
          cursor="pointer"
          style={{
            color: "#045993",
          }}
        >
          {text}
        </Text>
      ),
    },
    {
      title: "Người thực hiện",
      dataIndex: "executor",
      key: "executor",
    },
    {
      title: "Thao tác",
      dataIndex: "operation",
      key: "operation",
    },
    {
      title: "Thời gian thực hiện",
      dataIndex: "executionTime",
      key: "executionTime",
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      username: "nghiabui",
      executor: "Bùi Tuấn Nghĩa",
      operation: "Đăng nhập admin edubills",
      executionTime: "17/05/2023 17:40:25",
      stt: "1",
    },
    {
      key: "2",
      username: "nghiabui",
      executor: "Bùi Tuấn Nghĩa",
      operation: "Đăng nhập admin edubills",
      executionTime: "17/05/2023 17:40:25",
      stt: "2",
    },
    {
      key: "3",
      username: "nghiabui",
      executor: "Bùi Tuấn Nghĩa",
      operation: "Đăng nhập admin edubills",
      executionTime: "17/05/2023 17:40:25",
      stt: "3",
    },
  ];

  useEffect(() => {
    setDataSource(data);
  }, []);

  return (
    <Box>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
    </Box>
  );
};

export default TableManager;
