import { Box, Flex } from "@chakra-ui/react";
import { Button, Form, FormInstance, Input, Select } from "antd";
import MainButton from "components/button";
import { HSeparator } from "components/separator/Separator";
import React from "react";

const { Option } = Select;
const HeaderSearch = () => {
  const formRef = React.useRef<FormInstance>(null);

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onReset = () => {
    formRef?.current?.resetFields();
  };

  return (
    <Box width={"100%"}>
      <Form
        onFinish={onFinish}
        ref={formRef}
        style={{ maxWidth: "100%" }}
        layout="vertical"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Flex>
            <Box mr="2">
              <Form.Item name="username1" className="wrap-input">
                <Input placeholder="Tên tổ chức" className="custom-input" />
              </Form.Item>
            </Box>
            <Box mr="2">
              <Form.Item name="username2">
                <Input placeholder="Mã tổ chức" className="custom-input" />
              </Form.Item>
            </Box>
          </Flex>

          <Flex>
            <Box mr={2}>
              <Form.Item>
                <MainButton htmlType="submit" text="Tìm kiếm" />
              </Form.Item>
            </Box>

            <Form.Item>
              <Button
                className="custom-button button-danger"
                danger
                onClick={onReset}
              >
                Bỏ lọc
              </Button>
            </Form.Item>
          </Flex>
        </Flex>
      </Form>
      <HSeparator mb="4" />
    </Box>
  );
};

export default HeaderSearch;
