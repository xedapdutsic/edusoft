import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

interface DataType {
  key: string;
  stt: string;
  branchCode: string;
  branchName: string;
}

const current = 1;
const pageSize = 2;

const TableManager = () => {
  const history = useHistory();
  const [dataSource, setDataSource] = useState<DataType[]>([]);
  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  const handleNavigate = (id: string) => {
    history.push(`/admin/organization-detail/${id}`);
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Tên chi nhánh",
      dataIndex: "branchName",
      key: "branchName",
      render: (text, data) => {
        return (
          <Text
            onClick={() => handleNavigate(data.branchCode)}
            cursor="pointer"
            style={{
              color: "#045993",
            }}
          >
            {text}
          </Text>
        );
      },
    },
    {
      title: "Mã chi nhánh",
      dataIndex: "branchCode",
      key: "branchCode",
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      branchCode: "TXB96522",
      branchName: "Thanh Xuân",
      stt: "1",
    },
    {
      key: "2",
      branchCode: "TXB96522",
      branchName: "Thanh Xuân",
      stt: "2",
    },
    {
      key: "3",
      branchCode: "TXB96522",
      branchName: "Thanh Xuân",
      stt: "3",
    },
  ];

  useEffect(() => {
    setDataSource(data);
  }, []);

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: 50 người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
        >
          Thêm mới
        </Button>
      </Flex>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
    </Box>
  );
};

export default TableManager;
