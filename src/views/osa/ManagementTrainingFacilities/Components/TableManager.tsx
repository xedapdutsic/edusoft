import { Box, Button, Flex, Text } from "@chakra-ui/react";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import Table from "components/table";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

interface DataType {
  key: string;
  stt: string;
  facilityName: string;
  baseCode: string;
  sdt: string;
  email: string;
  fax: string;
  branch: string;
  address: string;
}

const current = 1;
const pageSize = 2;

const TableManager = () => {
  const history = useHistory();
  const [dataSource, setDataSource] = useState<DataType[]>([]);
  const [tableParams, setTableParams] = useState<{
    pagination?: TablePaginationConfig;
  }>({
    pagination: {
      current,
      pageSize,
    },
  });

  const handleNavigate = (id: string) => {
    history.push(`/admin/organization-detail/${id}`);
  };

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Tên cơ sở",
      dataIndex: "facilityName",
      key: "facilityName",
      render: (text, data) => {
        return (
          <Text
            onClick={() => handleNavigate(data.baseCode)}
            cursor="pointer"
            style={{
              color: "#045993",
            }}
          >
            {text}
          </Text>
        );
      },
    },
    {
      title: "Mã cơ sở",
      dataIndex: "baseCode",
      key: "baseCode",
    },
    {
      title: "Số điện thoại",
      dataIndex: "sdt",
      key: "sdt",
    },
    {
      title: "SĐT",
      dataIndex: "sdt",
      key: "sdt",
    },
    {
      title: "Fax",
      dataIndex: "fax",
      key: "fax",
    },
    {
      title: "Chi nhánh",
      dataIndex: "branch",
      key: "branch",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      facilityName: "Pisa Thanh Xuân",
      baseCode: "1234567890",
      sdt: "1234567890",
      email: "tuanhanh@gmail.com",
      fax: "8109439",
      branch: "Thanh Xuân",
      address: "23 Hoàng Ngân, Nhân chính, TX, HN",
      stt: "1",
    },
    {
      key: "2",
      facilityName: "Pisa Thanh Xuân",
      baseCode: "1234567890",
      sdt: "1234567890",
      email: "tuanhanh@gmail.com",
      fax: "8109439",
      branch: "Thanh Xuân",
      address: "23 Hoàng Ngân, Nhân chính, TX, HN",
      stt: "2",
    },
    {
      key: "3",
      facilityName: "Pisa Thanh Xuân",
      baseCode: "1234567890",
      sdt: "1234567890",
      email: "tuanhanh@gmail.com",
      fax: "8109439",
      branch: "Thanh Xuân",
      address: "23 Hoàng Ngân, Nhân chính, TX, HN",
      stt: "3",
    },
  ];

  useEffect(() => {
    setDataSource(data);
  }, []);

  return (
    <Box>
      <Flex py="3" pt="4" justifyContent="space-between">
        <Text>Tổng: 50 người dùng</Text>
        <Button
          colorScheme="whatsapp"
          backgroundColor={"#5FAD67"}
          borderRadius="20px"
        >
          Thêm mới
        </Button>
      </Flex>
      <Table
        columns={columns}
        dataSource={dataSource}
        tableParams={tableParams}
        setDataSource={setDataSource}
        setTableParams={setTableParams}
      />
    </Box>
  );
};

export default TableManager;
