import { Box, Flex } from "@chakra-ui/react";
import { Button, Form, FormInstance, Input, Select } from "antd";
import MainButton from "components/button";
import { HSeparator } from "components/separator/Separator";
import React from "react";

const { Option } = Select;
const HeaderSearch = () => {
  const formRef = React.useRef<FormInstance>(null);

  const onFinish = (values: any) => {
    console.log("Success:", values);
  };

  const onReset = () => {
    formRef?.current?.resetFields();
  };

  return (
    <Box width={"100%"}>
      <Form
        onFinish={onFinish}
        ref={formRef}
        style={{ maxWidth: "100%" }}
        layout="vertical"
      >
        <Flex alignItems="center" flexWrap="wrap">
          <Box mr="2">
            <Form.Item name="username1" className="wrap-input">
              <Input placeholder="Tên cơ sở" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="username2">
              <Input placeholder="Mã cơ sở" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="sdt">
              <Input placeholder="Số điện thoại" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="email">
              <Input placeholder="Email" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="Fax">
              <Input placeholder="Fax" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="Chi nhánh">
              <Input placeholder="branch" className="custom-input" />
            </Form.Item>
          </Box>
          <Box mr="2">
            <Form.Item name="Địa chỉ">
              <Input placeholder="address" className="custom-input" />
            </Form.Item>
          </Box>

          <Box mr={2}>
            <Form.Item>
              <MainButton htmlType="submit" text="Tìm kiếm" />
            </Form.Item>
          </Box>

          <Form.Item>
            <Button
              className="custom-button button-danger"
              danger
              onClick={onReset}
            >
              Bỏ lọc
            </Button>
          </Form.Item>
        </Flex>
      </Form>
      <HSeparator mb="4" />
    </Box>
  );
};

export default HeaderSearch;
