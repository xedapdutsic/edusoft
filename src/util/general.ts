// sét giá trị của một cookie nhất định
export const saveValueInCookie = (key: string, value: string) => {
  var currentDate = new Date();
  currentDate.setDate(currentDate.getDate() + 1);
  var expires = currentDate.toUTCString();
  document.cookie = `${key}=${value}; expires=${expires};`;
};

// đọc giá trị của một cookie nhất đinh
export const readValueCookie = (value: string) => {
  const cookies = document.cookie.split(";");
  const cookieObj: any = {};
  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i].trim().split("=");
    cookieObj[cookie[0]] = cookie[1];
  }
  return cookieObj[value];
};

export const formatMoney = (money: number) => {
  let _money = String(money);
  _money = _money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");

  return _money;
};

export const deleteCookie = (cookieName: string) => {
  document.cookie =
    cookieName + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
};
