import React, { useMemo } from "react";
import { Table as TableAndt } from "antd";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import { EnumPermission } from "util/Constant";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { Type_Initstate } from "store/appSlice";

interface IProps {
  dataSource: any[];
  columns: ColumnsType<any>;
  tableParams: {
    pagination?: TablePaginationConfig;
  };
  setDataSource: React.Dispatch<React.SetStateAction<any[]>>;
  setTableParams: React.Dispatch<
    React.SetStateAction<{
      pagination?: TablePaginationConfig;
    }>
  >;
}

const Table = (props: IProps) => {
  const { columns, dataSource, tableParams, setDataSource, setTableParams } =
    props;

  const appState = useSelector<RootState, Type_Initstate>((state) => state.app);
  const { appPermission } = appState;

  const backgroundBtnPagination = useMemo(() => {
    switch (appPermission) {
      case EnumPermission.ADMIN:
        return "pagination-admin";
      case EnumPermission.CSDT:
        return "pagination-csdt";
      case EnumPermission.OSA:
        return "pagination-osa";

      default:
        return "pagination-admin";
    }
  }, [appPermission]);

  const handleTableChange = (pagination: TablePaginationConfig) => {
    setTableParams({
      pagination,
    });
    // `dataSource` is useless since `pageSize` changed
    if (pagination.pageSize !== tableParams.pagination?.pageSize) {
      setDataSource([]);
    }
  };

  return (
    <>
      <TableAndt
        columns={columns}
        dataSource={dataSource}
        className={backgroundBtnPagination}
        onChange={handleTableChange}
        pagination={{
          position: ["bottomCenter"],
          // total: 10,
          ...tableParams.pagination,
        }}
      />
    </>
  );
};

export default Table;
