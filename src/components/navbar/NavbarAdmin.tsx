/* eslint-disable */
// Chakra Imports
import {
  Box,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Image,
  Spacer,
  Text,
} from "@chakra-ui/react";
import { SidebarResponsive } from "components/sidebar/Sidebar";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import routes from "routes";
import { RootState } from "store";
import { Type_Initstate } from "store/appSlice";
import { globalStyles } from "theme/styles";
import { EnumPermission } from "util/Constant";

export default function AdminNavbar(props: {
  secondary: boolean;
  message: string | boolean;
  brandText: { name: string; url: string };
  logoText: string;
  fixed: boolean;
  onOpen: (...args: any[]) => any;
}) {
  const appState = useSelector<RootState, Type_Initstate>((state) => state.app);
  const { authInfor, breadcrumb, appPermission } = appState;

  const { secondary, brandText } = props;

  const colorText = useMemo(() => {
    switch (appPermission) {
      case EnumPermission.ADMIN:
        return globalStyles.colors.permission.ADMIN;
      case EnumPermission.CSDT:
        return globalStyles.colors.permission.CSDT;
      case EnumPermission.OSA:
        return globalStyles.colors.permission.OSA;

      default:
        return globalStyles.colors.permission.ADMIN;
    }
  }, [appPermission]);

  // Here are all the props that may change depending on navbar's type or state.(secondary, variant, scrolled)
  let navbarFilter = "none";
  let navbarBackdrop = "blur(20px)";
  let navbarShadow = "none";
  let navbarBg = globalStyles.colors.bg_color[100];
  let navbarBorder = "transparent";
  let secondaryMargin = "0px";
  let paddingX = "15px";
  return (
    <Box
      boxShadow={navbarShadow}
      bg={navbarBg}
      borderColor={navbarBorder}
      filter={navbarFilter}
      backdropFilter={navbarBackdrop}
      backgroundPosition="center"
      backgroundSize="cover"
      borderWidth="1.5px"
      borderStyle="solid"
      transitionDelay="0s, 0s, 0s, 0s"
      transitionDuration=" 0.25s, 0.25s, 0.25s, 0s"
      transition-property="box-shadow, background-color, filter, border"
      transitionTimingFunction="linear, linear, linear, linear"
      alignItems={{ xl: "center" }}
      display={secondary ? "block" : "flex"}
      minH="75px"
      justifyContent={{ xl: "center" }}
      lineHeight="25.6px"
      mx="auto"
      mt={secondaryMargin}
      pb="8px"
      right={{ base: "12px", md: "30px", lg: "30px", xl: "30px" }}
      px={{
        sm: paddingX,
        md: "10px",
      }}
      ps={{
        xl: "12px",
      }}
      pt="8px"
      top={{ base: "12px", md: "16px", xl: "18px" }}
      w={{
        base: "calc(100vw - 6%)",
        md: "calc(100vw - 8%)",
        lg: "calc(100vw - 6%)",
        xl: "calc(100vw - 350px)",
        "2xl": "calc(100vw - 365px)",
      }}
    >
      <Flex
        w="100%"
        // flexDirection={{
        //   sm: "column",
        //   md: "row",
        // }}
        alignItems={{ xl: "center" }}
        // mb={gap}
      >
        <Box mb={{ sm: "8px", md: "0px" }}>
          <Breadcrumb>
            <BreadcrumbItem
              color={globalStyles.colors.text[202027]}
              fontSize="sm"
              mb="5px"
            >
              <BreadcrumbLink
                href="#"
                color={colorText}
                fontWeight="600"
                fontSize="15px"
              >
                Dashboard
              </BreadcrumbLink>
            </BreadcrumbItem>
            <BreadcrumbItem color={globalStyles.colors.text[202027]}>
              <BreadcrumbLink
                // href={`#${brandText.url}`}
                color={
                  breadcrumb.length !== 0
                    ? colorText
                    : globalStyles.colors.text[202027]
                }
                fontWeight="600"
                fontSize="15px"
              >
                {brandText.name}
              </BreadcrumbLink>
            </BreadcrumbItem>
            {breadcrumb.map((e, i) => (
              <BreadcrumbItem color={globalStyles.colors.text[202027]} key={i}>
                <BreadcrumbLink
                  href={`#${e.link}`}
                  color={
                    breadcrumb.length - 1 !== i
                      ? colorText
                      : globalStyles.colors.text[202027]
                  }
                  fontWeight="600"
                  fontSize="15px"
                >
                  {e.name}
                </BreadcrumbLink>
              </BreadcrumbItem>
            ))}
          </Breadcrumb>
        </Box>
        <Box mb={{ sm: "8px", md: "0px" }}>
          <SidebarResponsive routes={routes} />
        </Box>

        <Spacer />
        <Box display={"flex"} cursor="pointer">
          <Image
            borderRadius="full"
            boxSize="43px"
            src={require("../../assets/img/avatars/avatar.png")}
            alt="Avvatar"
          />
          <Box display={"flex"} flexDirection="column">
            <Text pl="2" className="text-18">
              {authInfor?.fullName}
            </Text>
            <Text pl="2" className="text-12">
              {authInfor?.accountType}
            </Text>
          </Box>
        </Box>
      </Flex>
    </Box>
  );
}
