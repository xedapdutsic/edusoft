import { RootState } from "store";
import { Button, ButtonProps } from "antd";
import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { EnumPermission } from "util/Constant";
import { Type_Initstate } from "store/appSlice";

interface IPButton extends ButtonProps {
  text: string;
  classButton?: string;
}

const MainButton = (props: IPButton) => {
  const { classButton, text, ...rest } = props;
  const appState = useSelector<RootState, Type_Initstate>((state) => state.app);

  const { appPermission } = appState;

  const backgroundBtn = useMemo(() => {
    switch (appPermission) {
      case EnumPermission.ADMIN:
        return "button-admin";
      case EnumPermission.CSDT:
        return "button-csdt";
      case EnumPermission.OSA:
        return "button-osa";

      default:
        return "button-admin";
    }
  }, [appPermission]);

  return (
    <Button
      {...rest}
      className={`custom-button ${classButton} ${backgroundBtn}`}
    >
      {text}
    </Button>
  );
};

export default MainButton;
