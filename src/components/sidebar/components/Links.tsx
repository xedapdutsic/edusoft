/* eslint-disable */

import { NavLink, useLocation } from "react-router-dom";
// chakra imports
import { Box, Flex, HStack, Text, useColorModeValue } from "@chakra-ui/react";
import { useMemo } from "react";
import { EnumPermission } from "util/Constant";
import { globalStyles } from "theme/styles";
import { useSelector } from "react-redux";
import { RootState } from "store";
import { Type_Initstate } from "store/appSlice";

export function SidebarLinks(props: { routes: RoutesType[] }) {
  const appState = useSelector<RootState, Type_Initstate>((state) => state.app);
  const { appPermission, activeRoute } = appState;

  //   Chakra color mode
  let location = useLocation();
  let activeColor = useColorModeValue("white", "white");
  let inactiveColor = useColorModeValue("text.100", "text.100");
  let activeIcon = useColorModeValue("white", "white");
  let textColor = useColorModeValue("text.100", "text.100");
  let brandColor = useColorModeValue("brand.500", "brand.400");

  const { routes } = props;

  // verifies if routeName is the one active (in browser input)
  const onActiveRoute = (routeName: string) => {
    return location.pathname.includes(routeName) || routeName === activeRoute;
  };

  const backgroundBtn = useMemo(() => {
    switch (appPermission) {
      case EnumPermission.ADMIN:
        return globalStyles.colors.permission.ADMIN;
      case EnumPermission.CSDT:
        return globalStyles.colors.permission.CSDT;
      case EnumPermission.OSA:
        return globalStyles.colors.permission.OSA;

      default:
        return globalStyles.colors.permission.ADMIN;
    }
  }, [appPermission]);

  // this function creates the links from the secondary accordions (for example auth -> sign-in -> default)
  const createLinks = (routes: RoutesType[]) => {
    return routes.map((route: RoutesType, index: number) => {
      if (
        route.layout === `/${appPermission?.toLocaleLowerCase()}` &&
        !route.hidden
      ) {
        return (
          <NavLink key={index} to={route.layout + route.path}>
            {route.icon ? (
              <Box>
                <HStack
                  spacing={
                    onActiveRoute(route.path.toLowerCase()) ? "22px" : "26px"
                  }
                  py="5px"
                  ps="10px"
                >
                  <Flex
                    w="100%"
                    alignItems="center"
                    justifyContent="center"
                    backgroundColor={
                      onActiveRoute(route.path.toLowerCase())
                        ? backgroundBtn
                        : "transparent"
                    }
                    p={2.5}
                    borderRadius={"19px"}
                  >
                    <Box
                      color={
                        onActiveRoute(route.path.toLowerCase())
                          ? activeIcon
                          : textColor
                      }
                      me="18px"
                    >
                      <i
                        className={`fa-solid ${route.icon} ${
                          onActiveRoute(route.path.toLowerCase())
                            ? "active-color"
                            : "inactive-color"
                        }`}
                      />
                    </Box>
                    <Text
                      me="auto"
                      fontSize={"16px"}
                      color={
                        onActiveRoute(route.path.toLowerCase())
                          ? activeColor
                          : textColor
                      }
                      fontWeight={"bold"}
                    >
                      {route.name}
                    </Text>
                  </Flex>
                  <Box
                    h="36px"
                    w="4px"
                    bg={
                      onActiveRoute(route.path.toLowerCase())
                        ? brandColor
                        : "transparent"
                    }
                    borderRadius="5px"
                  />
                </HStack>
              </Box>
            ) : (
              <Box>
                <HStack
                  spacing={
                    onActiveRoute(route.path.toLowerCase()) ? "22px" : "26px"
                  }
                  py="5px"
                  ps="10px"
                >
                  <Text
                    me="auto"
                    color={
                      onActiveRoute(route.path.toLowerCase())
                        ? activeColor
                        : inactiveColor
                    }
                    fontWeight={
                      onActiveRoute(route.path.toLowerCase())
                        ? "bold"
                        : "normal"
                    }
                  >
                    {route.name}
                  </Text>
                  <Box h="36px" w="4px" bg="brand.400" borderRadius="5px" />
                </HStack>
              </Box>
            )}
          </NavLink>
        );
      }
    });
  };
  //  BRAND
  return <>{createLinks(routes)}</>;
}

export default SidebarLinks;
