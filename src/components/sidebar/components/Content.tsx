// chakra imports
import { Box, Flex, Link, Stack, Text } from "@chakra-ui/react";
//   Custom components
import Brand from "components/sidebar/components/Brand";
import Links from "components/sidebar/components/Links";

// FUNCTIONS

function SidebarContent(props: { routes: RoutesType[] }) {
  const { routes } = props;
  // SIDEBAR
  return (
    <Flex direction="column" height="100%" pt="25px" borderRadius="30px">
      <Brand isCenter={true} />
      <Stack direction="column" mt="8px" mb="auto">
        <Box ps="20px" pe={{ lg: "16px", "2xl": "16px" }}>
          <Links routes={routes} />
        </Box>
      </Stack>
      <Link
        href="#/auth/login"
        ps="20px"
        pe={{ lg: "16px", "2xl": "20px" }}
        mt="60px"
        mb="40px"
        borderRadius="30px"
        cursor="pointer"
      >
        <Text
          className="btn-logout"
          textAlign={"center"}
          display={"flex"}
          justifyContent="center"
          flexDirection={"row"}
        >
          <Box mr="3">
            <i className="fa-solid fa-right-from-bracket btn-logout"></i>
          </Box>{" "}
          Log out
        </Text>
      </Link>
    </Flex>
  );
}

export default SidebarContent;
