// Chakra imports
import { Box, Flex } from "@chakra-ui/react";
import { HorizonLogo } from "components/icons/HorizonLogo";
import { Text } from "@chakra-ui/react";
import { FC } from "react";

// Custom components

interface P {
  isCenter?: boolean;
}

const SidebarBrand: FC<P> = ({ isCenter }: P) => {
  return (
    <Flex
      alignItems={isCenter ? "center" : "flex-start"}
      flexDirection="column"
      cursor={"pointer"}
    >
      <Box
        mb="25px"
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <HorizonLogo />
        <Box display="flex" flexDirection={"column"}>
          <Text
            ml={3}
            style={{
              color: "#9C9C9C",
              fontWeight: 700,
              fontSize: "26px",
              lineHeight: "1",
            }}
          >
            Admin Edubills
          </Text>

          <Box ml={3} lineHeight="1">
            Education
          </Box>
        </Box>
      </Box>
      {/* <HSeparator mb="20px" /> */}
    </Flex>
  );
};

export default SidebarBrand;
