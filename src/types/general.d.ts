export type PaginationType = {
  totalPages: number;
  totalRows: number;
};
export type ResponseBase<T = any, TStatus = boolean> = {
  code: number;
} & (TStatus extends true
  ? {
      data: T;

      status: true;
    }
  : {
      status: false;

      msg?: string | null;
    });

export type LocalStorageType = {
  orgName: string;
  orgCode: string;
  _id: string;
  status: number;
};

export type OrganizationInput = {
  address: string;
  email: string;
  endTime: number;
  orgCode: string;
  orgName: string;
  phone: string;
  startTime: number;
  status: string;
};
