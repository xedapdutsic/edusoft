import { NetWorkService } from "util/API";
import {
  AuthAdminLoginResponsive,
  AuthCsdtResponsive,
  AuthInputDataType,
} from "./Auth";

const LOGIN_ADMIN = "/auth/admin/login";
const LOGIN_SCHOOL = "/auth/school/login";
const LOGIN_ORGANIZATION = "/auth/organization/login";
const ADMIN_DETAIL = "/auth/admin/detail";
const ORGANIZATION_DETAIL = "/auth/organization/detail";
const SCHOOL_DETAIL = "/auth/school/detail";

const { Post, Get } = NetWorkService;

export default class AuthService {
  static LoginAdmin = async (data: AuthInputDataType) => {
    return await Post<AuthAdminLoginResponsive>({
      url: LOGIN_ADMIN,
      body: { ...data },
    });
  };
  static LoginOrganization = async (data: AuthInputDataType) => {
    return await Post<AuthAdminLoginResponsive>({
      url: LOGIN_ORGANIZATION,
      body: { ...data },
    });
  };
  static LoginSchool = async (data: AuthInputDataType) => {
    return await Post<AuthAdminLoginResponsive>({
      url: LOGIN_SCHOOL,
      body: { ...data },
    });
  };
  static GetAdminDetail = async () => {
    return await Get<AuthAdminLoginResponsive>({ url: ADMIN_DETAIL });
  };
  static GetOrganizationDetail = async () => {
    return await Get<AuthAdminLoginResponsive>({ url: ORGANIZATION_DETAIL });
  };
  static GetSchoolDetail = async () => {
    return await Get<AuthCsdtResponsive>({ url: SCHOOL_DETAIL });
  };
}
