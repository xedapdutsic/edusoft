import { EnumPermission } from "util/Constant";

export interface AuthInputDataType {
  username: string;
  password: string;
}

export interface AuthInfor {
  accountType: EnumPermission;
  createdAt: string;
  email: string;
  fullName: string;
  personnelCode: string;
  phone: string;
  roles: string[];
  username: string;
  __v: 0;
  _id: string;
  idSchool: string;
  orgCode: string;
  schoolCode: string;
}

export interface AdminLoginResponsiveDetail {
  accountType: string;
  createdAt: string;
  email: string;
  exp: number;
  fullName: string;
  iat: number;
  personnelCode: string;
  phone: string;
  roles: string[];
  status: number;
  username: string;
  _id: string;
}
export interface AuthAdminLoginResponsive {
  data: AuthAdminInfor;
  access_token: string;
  code: number;
}
export interface AuthCsdtResponsive {
  data: {
    exp: number;
    iat: number;
    sub: string;
    user: AuthInfor;
  };
  code: number;
}

export interface AuthInfor {
  username: string;
  email: string;
  role: string;
}
