import { PaginationType } from "types/general";

export interface InputCreate {
  email: string;
  orgCode: string;
  orgName: string;
  phone: string;
  address: string;
  roles: string[];
  startTime: number;
  endTime: number;
  username: string;
  fullName: string;
  emailAdmin: string;
  phoneAdmin: string;
  addressAdmin: string;
}

export interface OrganizationItem {
  address: string;
  createdAt: string;
  email: string;
  endTime: number;
  orgCode: string;
  orgName: string;
  phone: string;
  startTime: number;
  status: number;
  _id: string;
}

export interface ListOrganizationResponsive {
  data: {
    pagination: PaginationType;
    records: OrganizationItem[];
  };
  code: number;
}

export interface OrganizationDetailResponsive {
  data: OrganizationDetail;
  code: number;
}
export interface OrganizationDetail {
  address: string;
  createdAt: string;
  email: string;
  endTime: number;
  listSchool: any[];
  orgCode: string;
  orgName: string;
  phone: string;
  startTime: number;
  status: number;
  _id: string;
  statusCode: number;
}
