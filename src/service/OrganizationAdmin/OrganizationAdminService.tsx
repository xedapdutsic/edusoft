import { NetWorkService } from "util/API";
import {
  ListOrganizationResponsive,
  OrganizationDetailResponsive,
} from "./Organization";

const ORGANIZATION = "/organization";
const ORGANIZATION_UPDATE = "/organization/update";
const ORGANIZATION_CREATE = "/organization/create";
const SCHOOL = "/school";
const { Get, Post } = NetWorkService;

export default class OrganizationAdminService {
  static GetListOrganizationAdmin = async (query: string) => {
    return await Get<ListOrganizationResponsive>({
      url: ORGANIZATION + query,
    });
  };
  static CreateOrganization = async (data: any) => {
    return await Post<any>({
      url: ORGANIZATION_CREATE,
      body: { ...data },
    });
  };
  static GetSchool = async (id: string, query: string) => {
    return await Get<any>({
      url: ORGANIZATION + `/${id}` + SCHOOL + query,
    });
  };
  static OrganizationDetail = async (id: string) => {
    return await Get<OrganizationDetailResponsive>({
      url: ORGANIZATION + `/${id}/detail`,
    });
  };
  static OrganizationUpdate = async (data: any) => {
    return await Post<OrganizationDetailResponsive>({
      url: ORGANIZATION_UPDATE,
      body: { ...data },
    });
  };
}
