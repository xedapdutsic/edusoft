import React from "react";
import ReactDOM from "react-dom";
import "./assets/css/App.css";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import AuthLayout from "./layouts/auth";
import AdminLayout from "./layouts/admin";
import OsaLayout from "./layouts/osa";
import CsdtLayout from "./layouts/csdt";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "./theme/theme";
import { store } from "./store";
import { Provider } from "react-redux";
import { ADMIN, AUTH, CSDT, DEFAULT_ROUTER, OSA } from "util/Constant";

ReactDOM.render(
  <Provider store={store}>
    <ChakraProvider theme={theme}>
      <React.StrictMode>
        <HashRouter>
          <Switch>
            <Route path={AUTH} component={AuthLayout} />
            <Route path={ADMIN} component={AdminLayout} />
            <Route path={OSA} component={OsaLayout} />
            <Route path={CSDT} component={CsdtLayout} />
            <Redirect from="/" to={`${ADMIN}${DEFAULT_ROUTER}`} />
          </Switch>
        </HashRouter>
      </React.StrictMode>
    </ChakraProvider>
  </Provider>,
  document.getElementById("root")
);
