// Admin Imports
import MainDashboard from "views/admin/default";
import DataTables from "views/admin/UserManager";
import SignInCentered from "views/auth/signIn";
// Auth Imports
import Login from "views/auth/Login/Login";
import OrganizationManagement from "views/admin/OrganizationManagement";
import OrganizationDetail from "views/admin/OrganizationDetail";
import UserManagerDetail from "views/admin/UserManagerDetail";
import OperationLogOsa from "views/osa/OperationLog";
import Report from "views/admin/Report";
import ReportOsa from "views/osa/Report";
import BranchManagement from "views/osa/BranchManagement";
import ManagementTrainingFacilities from "views/osa/ManagementTrainingFacilities";
import OrganizationalUserManagement from "views/osa/OrganizationalUserManagement";
import OperationLog from "views/admin/OperationLog";
import { ADMIN, AUTH, OSA, CSDT } from "util/Constant";
import AddNewOrganization from "views/admin/AddNewOrganization";
import TrainingFacilities from "views/admin/AddNewOrganization/TrainingFacilities";
import LoginAccount from "views/admin/AddNewOrganization/LoginAccount";

const routes = [
  // ------ ADMIN EDUBILLS (ROOT USER) ------- //
  {
    name: "Dashboard",
    layout: `${ADMIN}`,
    path: "/default",
    component: MainDashboard,
    icon: "fa-solid fa-house",
  },
  {
    name: "Quản lý tổ chức",
    layout: `${ADMIN}`,
    path: "/organization-management",
    component: OrganizationManagement,
    icon: "fa-chart-column",
  },
  {
    name: "Quản lý người dùng EduBills",
    layout: `${ADMIN}`,
    path: "/user-management",
    component: DataTables,
    icon: "fa-chart-column",
  },
  {
    name: "Chi tiết tổ chức",
    layout: `${ADMIN}`,
    path: "/organization-detail/:id",
    component: OrganizationDetail,
    icon: "fa-chart-column",
    hidden: true,
  },
  {
    name: "Chi tiết tổ chức",
    layout: `${ADMIN}`,
    path: "/user-detail/:id",
    component: UserManagerDetail,
    icon: "fa-chart-column",
    hidden: true,
  },
  // === them moi to chuc ===== //
  {
    name: "Quản lý tổ chức",
    path: "/add-new-organization",
    layout: `${ADMIN}`,
    component: AddNewOrganization,
    icon: "fa-chart-column",
    hidden: true,
  },
  {
    name: "Quản lý tổ chức",
    path: "/training-facilities",
    layout: `${ADMIN}`,
    component: TrainingFacilities,
    icon: "fa-chart-column",
    hidden: true,
  },
  {
    name: "Quản lý tổ chức",
    path: "/login-account",
    layout: `${ADMIN}`,
    component: LoginAccount,
    icon: "fa-chart-column",
    hidden: true,
  },

  // === end them moi to chuc ===== //

  // === co so ===== //
  {
    name: "Quản lý tổ chức",
    path: "/login-account",
    layout: `${ADMIN}`,
    component: LoginAccount,
    icon: "fa-chart-column",
    hidden: true,
  },
  // === end co so ===== //

  {
    name: "Nhật ký thao tác",
    layout: `${ADMIN}`,
    path: "/operation-log",
    component: OperationLog,
    icon: "fa-chart-column",
  },
  {
    name: "Báo cáo",
    layout: `${ADMIN}`,
    path: "/report",
    component: Report,
    icon: "fa-chart-column",
  },

  // --------------- osa --------------- //
  {
    name: "Dashboard",
    layout: `${OSA}`,
    path: "/default",
    component: MainDashboard,
    icon: "fa-solid fa-house",
  },

  {
    name: "Quản lý chi nhánh",
    layout: `${OSA}`,
    path: "/branch-management",
    component: BranchManagement,
    icon: "fa-chart-column",
  },
  {
    name: "Quản lý cơ sở đào tạo",
    layout: `${OSA}`,
    path: "/management-training-facilities",
    component: ManagementTrainingFacilities,
    icon: "fa-chart-column",
  },
  {
    name: "Quản lý người dùng tổ chức",
    layout: `${OSA}`,
    path: "/organizational-user-management",
    component: OrganizationalUserManagement,
    icon: "fa-chart-column",
  },
  {
    name: "Nhật ký thao tác",
    layout: `${OSA}`,
    path: "/operation-log-osa",
    component: OperationLogOsa,
    icon: "fa-chart-column",
  },
  {
    name: "Báo cáo",
    layout: `${OSA}`,
    path: "/report",
    component: ReportOsa,
    icon: "fa-chart-column",
  },

  // --------------- auth --------------- //
  {
    name: "sign in",
    layout: `${AUTH}`,
    path: "/sign-in",
    component: SignInCentered,
    icon: "fa-chart-column",
  },
  {
    name: "login",
    layout: `${AUTH}`,
    path: "/login",
    component: Login,
    icon: "fa-chart-column",
  },

  // --------------- CSDT --------------- //
  {
    name: "Dashboard",
    layout: `${CSDT}`,
    path: "/default",
    component: MainDashboard,
    icon: "fa-solid fa-house",
  },
  {
    name: "Báo cáo",
    layout: `${CSDT}`,
    path: "/report",
    component: ReportOsa,
    icon: "fa-chart-column",
  },
];

export default routes;
